package ru.seryakov.practice101;

import java.util.ArrayList;
import java.util.List;

public class ThreadRunner {
/*    public static void main(String[] args) {
        ThreadExample threadExample = new ThreadExample();

        List<Thread> runnableList = runTenThreads(threadExample);

        for (Thread runnable : runnableList) {
            try {
                runnable.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("finish");

    }*/

    public static List<Thread> runTenThreads(Thread threadExample) {
        List<Thread> runnableList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(threadExample);
            thread.start();
            runnableList.add(thread);
        }
        return runnableList;
    }
}
