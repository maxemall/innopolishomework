$(document).ready(function () {
    var cookie = $.cookie('visible_search');
    if(cookie == undefined || cookie == "false") {
        $("#searchForm").hide();
    } else {
        $("#searchForm").show();
    }

    //Отображение/скрытие фильтров в списке студентов
    $("#showFilters").click(function () {
        $("#searchForm").toggle();
        cookie = $.cookie('visible_search');
        if(cookie == undefined || cookie == "false") {
            $.cookie('visible_search', "true");
        } else {
            $.cookie('visible_search', "false");
        }
    });

    $(".filters").click(function () {
        $.cookie('visible_search', "true");
    });

    $("#clearButton").click(function () {
        $.cookie('visible_search', "true");
        window.location.href = location.protocol + '//' + location.host + location.pathname;
    });

    $("#clearProjectEventCriteria").click(function () {
        $.cookie('visible_search', "true");
        var params = getQueryParameters();
        // if(params.startDateFor = "") {
            params.startDateWith = "";
            params.startDateFor = "";
            setQueryParameters(params);
    });
});

function getQueryParameters() {
    var queryString = location.search.slice(1),
        params = {};

    queryString.replace(/([^=]*)=([^&]*)&*/g, function (_, key, value) {
        params[key] = value;
    });

    return params;
}

function setQueryParameters(params) {
    var query = [],
        key, value;

    for(key in params) {
        if(!params.hasOwnProperty(key)) continue;
        value = params[key];
        query.push(key + "=" + value);
    }

    location.search = query.join("&");
}