$(document).ready(function () {
    //Виджет для выбора даты
    $('.date-picker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: 'ru'
    });

    //Отправитель родительской формы по клику
    $('.form-submitter-on-click').on('click', function () {
        $(this).parents('form').submit();
    });

    //Установка статуса студента
    $('#isActive').on('click', function () {
        var value = $('#isActive').prop("checked");
        var object = {
            isActive: value,
            id: $('#studentId').val()
        };
        $.ajax({
            url: '/students/edit/status_is_active/',
            type: 'POST',
            data: object,
            success: function (data) {
                var status = (data.isActive == true) ? '"Активный"' : '"Неактивный"';
                alert('Статус студента был успешно изменен на ' + status);
            },
            error: function () {
                alert('Произошла ошибка, попробуйте повторить операцию позже');
            }
        });
        if (value) {
            this.prop("checked", true);
        } else {
            this.removeProp("checked");
        }
        return false;
    })

    //Установка статуса студента для проектов
    $('#projectActive').on('click', function () {
        var value = $('#projectActive').prop("checked");
        var object = {
            projectActive: value,
            id: $('#studentId').val()
        };
        $.ajax({
            url: '/students/edit/status_project_active/',
            type: 'POST',
            data: object,
            success: function (data) {
                var status = (data.projectActive == true) ? '"Активный"' : '"Неактивный"';
                alert('Статус проектов был успешно изменен на ' + status);
            },
            error: function () {
                alert('Произошла ошибка, попробуйте повторить операцию позже');
            }
        });
        if (value) {
            this.prop("checked", true);
        } else {
            this.removeProp("checked");
        }
        return false;
    });

});

