/**
 * Created by Zelenkov Sergey on 29.04.17.
 */
$(document).ready(function () {
    Update();

    document.getElementById("periodical-type").addEventListener("click", function () {
        Update();
    });

    document.getElementById("periodical").addEventListener("click", function () {
        ShowPeriodityOptions()
    });

    document.getElementById("select-interval").addEventListener("change", function () {
        Update();
    });

    document.getElementById("start-date").addEventListener("changeDate", function () {
        alert("working");
        Update();
    });

    document.getElementById("day-of-month").addEventListener("change", function () {
        Update();
    });

    document.getElementById("day-of-week").addEventListener("change", function () {
        Update();
    });

});

function ShowPeriodityOptions() {
    var x = $("#periodical").is(":checked");

    // Если дата начала не установлена - указываем текущую дату
    var date = document.getElementById("start-date").value;
    if (date == "") {
        document.getElementById("start-date").value = new Date().toISOString().substring(0, 10);
    }

    if (x != false) {
        document.getElementById("tabad").style.display = 'block';
    }
    else {
        document.getElementById("tabad").style.display = 'none';
    }
}

function Update() {
    ShowPeriodityOptions();
    var e = document.getElementById("periodical-type");
    var interval = document.getElementById("select-interval");
    var intervalValue = interval.options[interval.selectedIndex].value;
    var dayOfMonth = document.getElementById("day-of-month").checked;
    var currDate = ParseDate(document.getElementById("start-date").value);
    var reportText;

    switch (e.selectedIndex) {
        case 0:
//          каждый день
            document.getElementById("days").style.display = 'none';
            document.getElementById("interval").style.display = 'table-row';
            document.getElementById("interval-unit").innerHTML = "дн.";
            if (intervalValue == 1) {
                reportText = "Каждый день";
            }
            else {
                reportText = "Кажд. " + intervalValue + " дн.";
            }

            break;
        case 1:
//          каждый будний день
            document.getElementById("days").style.display = 'none';
            document.getElementById("interval").style.display = 'none';
            reportText = "Каждую неделю – по рабочим дням";
            break;
        case 2:
//          каждую неделю
            document.getElementById("month-days").style.display = 'none';
            document.getElementById("days").style.display = 'table-row';
            document.getElementById("ned-days").style.display = 'inline';
            document.getElementById("interval").style.display = 'table-row';
            document.getElementById("interval-unit").innerHTML = "нед.";
            if (intervalValue == 1) {
                reportText = "Каждую неделю " + getCheckedBoxes("daysOfWeek");
            }
            else {
                reportText = "кажд. " + intervalValue + " нед. " + getCheckedBoxes("daysOfWeek");
            }
            break;
        case 3:
//          каждый месяц
            document.getElementById("interval").style.display = 'table-row';
            document.getElementById("ned-days").style.display = 'none';
            document.getElementById("month-days").style.display = 'inline';
            document.getElementById("days").style.display = 'table-row';
            document.getElementById("interval-unit").innerHTML = "мес.";

            if (intervalValue == 1) {
                if (dayOfMonth) {
                    reportText = "Каждый месяц " + (ParseDate(document.getElementById("start-date").value)).getDate() + " числа";
                } else {
                    reportText = "Каждый месяц " + weekAndDay(ParseDate(document.getElementById("start-date").value)) + " месяца";
                }
            }
            else {
                reportText = "Кажд. " + intervalValue + " мес. " + getCheckedBoxes("days-of-week");
                if (dayOfMonth) {
                    reportText = "Кажд. " + intervalValue + " мес. " + (ParseDate(document.getElementById("start-date").value)).getDate() + " числа";
                } else {
                    reportText = "Кажд. " + intervalValue + " мес. " + weekAndDay(ParseDate(document.getElementById("start-date").value)) + " месяца";
                }
            }
            break;
        case 4:
//          каждый год
            document.getElementById("interval").style.display = 'table-row';
            document.getElementById("days").style.display = 'none';
            document.getElementById("interval-unit").innerHTML = "г.";
            if (intervalValue == 1) {
                reportText = "Каждый год " + currDate.getDate() + " " + monthName(currDate);
            } else {
                reportText = "Кажд. " + intervalValue + " года (лет) " + currDate.getDate() + " " + monthName(currDate);
            }
            break;
        default:
            reportText = " Не установлено";
    }
    document.getElementById("periodity").value = reportText;
    console.log("selected index = " + e.selectedIndex);
}

function getCheckedBoxes(chkboxName) {
    var checkboxes = document.getElementsByName(chkboxName);
    var checkboxesChecked = [];
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            checkboxesChecked.push(" " + checkboxes[i].title);
        }
    }
    if (checkboxesChecked.length == 0) {
        checkboxesChecked.push(weekday[ParseDate(document.getElementById("start-date").value).getDay()]);
    }
    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

$(document).on('change', 'input[type=checkbox]', function (e) {
    Update();
});

function ParseDate(datestr) {
    var dateParts = datestr.split("-");
    var date = new Date(dateParts);
    return date;
}

var weekday = new Array(7);
weekday[0] = " Воскресение";
weekday[1] = " Понедельник";
weekday[2] = " Вторник";
weekday[3] = " Среда";
weekday[4] = " Четверг";
weekday[5] = " Пятница";
weekday[6] = " Субота";

var month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

function monthName(date) {
    return month[date.getMonth()];
}

function weekAndDay(date) {
    var prefixes = ['первой', 'второй', 'третьей', 'четвертой', 'последней'];

    return weekday[date.getDay()] + ' ' + prefixes[0 | date.getDate() / 7] + " недели";
}
