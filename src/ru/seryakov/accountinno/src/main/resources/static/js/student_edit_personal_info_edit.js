$(document).ready(function() {
    $("#student_personal_info_edit_save").click(function(){
        var student = {
            id: $('#studentId').val(),
            firstName: $('#input_FirstName').val(),
            middleName: $('#input_MiddleName').val(),
            lastName: $('#input_LastName').val(),
            gender: $('#gender_input').val(),
            birthDate: $('#birth_date_input').val(),
            birthPlace: {
                id: $('#birth_place_id_input').val(),
                city: $('#birth_place_input').val()
            },
            email: $('#email_input').val(),
            contactNumber: $('#contact_input').val(),
            status: $('#status_input').val(),
            passportSeria: $('#passport_seria_input').val(),
            passportNumber: $('#passport_number_input').val(),
            passportIssuedBy: $('#passport_issued_by_input').val(),
            passportIssueDate: $('#passport_issue_date_input').val(),
            reason: $('#reason_input').val(),
            addressPlan: $('#address_plan_input').val(),
            addressFact: $('#address_fact_input').val(),
        };

        console.log(student);

        $.ajax({
            url:'http://' + window.location.host + '/students/edit/personal_info/',
            type:"POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(student),
            success: function(response){
                alert('Изменения сохранены');
            },
            error: function (xhr) {
                alert('Ошибка редактирование: ' + xhr.responseText)
            }
        });
    });
});