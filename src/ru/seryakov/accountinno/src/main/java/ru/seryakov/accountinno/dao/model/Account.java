package ru.seryakov.accountinno.dao.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private Integer id;
    private String fullName;
    private BigDecimal balance;
    private Boolean active;
}
