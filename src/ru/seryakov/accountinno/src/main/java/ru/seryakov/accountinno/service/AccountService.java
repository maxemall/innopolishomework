package ru.seryakov.accountinno.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.seryakov.accountinno.dao.model.Account;
import ru.seryakov.accountinno.dao.repository.AccountRepository;
import ru.seryakov.accountinno.service.dto.AccountDto;
import ru.seryakov.accountinno.service.dto.TransferDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ModelMapper modelMapper;

    public List<AccountDto> findAll() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream().map(account -> modelMapper.map(account, AccountDto.class)).collect(Collectors.toList());
    }

    public AccountDto findById(Integer id) {
        return modelMapper.map(accountRepository.findById(id), AccountDto.class);
    }

    public AccountDto addMoney(Integer id, BigDecimal value) {
        Account account = accountRepository.addMoney(id, value);
        return modelMapper.map(account, AccountDto.class);
    }


    @Transactional
    public boolean transferFounds(TransferDto transferDto) {
        return accountRepository.transferMoney(transferDto);
    }
}
