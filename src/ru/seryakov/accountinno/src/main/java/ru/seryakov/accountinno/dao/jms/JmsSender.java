package ru.seryakov.accountinno.dao.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.Message;
import java.text.MessageFormat;
import java.util.UUID;

@Log4j2
@Component
public class JmsSender {

    @Autowired
    private ConnectionFactory connectionFactory;

    private JmsTemplate jmsTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        this.jmsTemplate = new JmsTemplate(connectionFactory);
    }

    public void sendMessage(String queueName, Object sending, UUID correlationId) throws JsonProcessingException {

        String messageText = objectMapper.writeValueAsString(sending);
        try {
            jmsTemplate.send(queueName, session -> {
                Message message = session.createTextMessage(messageText);
                message.setJMSCorrelationID(correlationId.toString());
                return message;
            });
        } catch (JmsException e) {
            log.warn("Error on sending message" + messageText, e);
        }
        log.info(MessageFormat.format("Sent: {0} Queue: {1}", messageText, queueName));
    }
}
