package ru.seryakov.accountinno.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class TransferDto {
    private Integer accountFrom;
    private Integer accountTo;
    private BigDecimal value;
}
