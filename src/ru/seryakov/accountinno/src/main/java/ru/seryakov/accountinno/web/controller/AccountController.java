package ru.seryakov.accountinno.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.seryakov.accountinno.dao.model.Account;
import ru.seryakov.accountinno.dao.repository.AccountRepository;
import ru.seryakov.accountinno.service.AccountService;
import ru.seryakov.accountinno.service.dto.TransferDto;

@Controller
public class AccountController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountService accountService;


    @GetMapping("/addAccount")
    public String accountForm(ModelMap map) {
        accountService.findAll();
        return "open_account";
    }

    @GetMapping("/addMoney")
    public String showAccounts(ModelMap map) throws JsonProcessingException {
        map.addAttribute("accountList", accountService.findAll());
        return "add_money";
    }

    @PostMapping("/addAccount")
    public String update(@ModelAttribute Account account, Model model) {
        model.addAttribute("success", true);
        return "open_account";
    }

    @PostMapping("/addMoney")
    public String addMoney(@ModelAttribute Account account, Model model) {
        model.addAttribute("accountList", accountRepository.findAll());
        model.addAttribute("account", accountRepository.addMoney(account.getId(), account.getBalance()));
        model.addAttribute("success", true);
        return "add_money";
    }

    @GetMapping("/transferMoney")
    public String showTransfer(ModelMap map) {
        map.addAttribute("accountList", accountRepository.findAll());
        return "transfer_money";
    }

    @PostMapping("/transferMoney")
    public String addTransfer(@ModelAttribute TransferDto transferDto, Model model) {
        if (accountService.transferFounds(transferDto)) {
            model.addAttribute("success", true);
        } else model.addAttribute("error", true);
        model.addAttribute("accountList", accountRepository.findAll());
        model.addAttribute("account", accountRepository.findById(transferDto.getAccountTo()));
        return "transfer_money";
    }


}
