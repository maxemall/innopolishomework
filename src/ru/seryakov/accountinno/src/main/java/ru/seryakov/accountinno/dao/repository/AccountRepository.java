package ru.seryakov.accountinno.dao.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.seryakov.accountinno.dao.jms.JmsReceiver;
import ru.seryakov.accountinno.dao.jms.JmsSender;
import ru.seryakov.accountinno.dao.model.Account;
import ru.seryakov.accountinno.service.dto.TransferDto;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Log4j2
@Component
public class AccountRepository {

    private static final String ACCOUNT_REQUEST_QUEUE = "account.in";
    private static final String ACCOUNT_RESPONSE_QUEUE = "account.out";
    private static final String MONEY_REQUEST_QUEUE = "money.in";
    private static final String MONEY_RESPONSE_QUEUE = "money.out";
    private static final String TRANSFER_REQUEST_QUEUE = "transfer.in";
    private static final String TRANSFER_RESPONSE_QUEUE = "transfer.out";

    @Autowired
    JmsSender jmsSender;

    @Autowired
    JmsReceiver jmsReceiver;

    @Autowired
    ObjectMapper objectMapper;

    public List<Account> findAll() {

        List<Account> resultList = null;
        String response = sendFindMessage(null);
        try {
            resultList = objectMapper.readValue(response,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, Account.class));
        } catch (IOException e) {
            log.error("Deserialize list account error " + e.getMessage());
        }
        return resultList;
    }

    private String sendFindMessage(Object message) {
        UUID uuid = UUID.randomUUID();
        try {
            jmsSender.sendMessage(ACCOUNT_REQUEST_QUEUE, message, uuid);
        } catch (JsonProcessingException e) {
            log.error("sending findAll error" + e.getMessage());
        }
        return jmsReceiver.receiveMessage(ACCOUNT_RESPONSE_QUEUE, uuid);
    }

    public Account findById(Integer id) {
        Account account = null;
        String response = sendFindMessage(id);
        try {
            account = objectMapper.readValue(response, Account.class);
        } catch (IOException e) {
            log.error("Deserialize account error " + e.getMessage());
        }
        return account;
    }

    public Account addMoney(Integer id, BigDecimal value) {
        Account account = findById(id);
        account.setBalance(account.getBalance().add(value));
        UUID uuid = UUID.randomUUID();
        try {
            jmsSender.sendMessage(MONEY_REQUEST_QUEUE, account, uuid);
        } catch (JsonProcessingException e) {
            log.error("Serialize account error " + e.getMessage());
        }
        String strMessage = jmsReceiver.receiveMessage(MONEY_RESPONSE_QUEUE, uuid);
        try {
            return objectMapper.readValue(strMessage, Account.class);
        } catch (IOException e) {
            log.error("Deserialize account error " + e.getMessage());
            return null;
        }
    }

    public boolean transferMoney(TransferDto transferDto) {
        UUID uuid = UUID.randomUUID();
        try {
            jmsSender.sendMessage(TRANSFER_REQUEST_QUEUE, transferDto, uuid);
        } catch (JsonProcessingException e) {
            log.error("sending transfer error" + e.getMessage());
        }
        return !jmsReceiver.receiveMessage(TRANSFER_RESPONSE_QUEUE, uuid).equals("false");

    }
}
