package ru.seryakov.practice105;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger timer = new AtomicInteger(0);
        Tick tick = new Tick(timer);
        tick.start();
        Sout sout = new Sout(timer, 5);
        sout.start();


/*        try {
            sout.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}
