package ru.seryakov.practice81.practice83;

import ru.seryakov.practice81.Testable;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class AnnotationTestHandler implements InvocationHandler {
    private Testable testable;

    public AnnotationTestHandler(Testable testable) {
        this.testable = testable;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        boolean logged = false;
        for (Annotation annotation : testable.getClass().getAnnotations()) {
            if ("Logged".equals(annotation.annotationType().getSimpleName()))
            {
                logged = true;
            }
        }

        long startTime = System.currentTimeMillis();
        if (logged) System.out.println("StartTime:" + startTime);
        Object result =  method.invoke(testable, args);
        long stopTime = System.currentTimeMillis();

        long time = stopTime - startTime;
        if (logged) System.out.println("Time:" + time);
        return result;
    }
}
