package ru.seryakov.practice81;

import ru.seryakov.practice81.practice83.AnnotationTestHandler;

import java.lang.reflect.Proxy;

public class TestApp {

    public static void main(String[] args) {
        Testable testIt = new TestClass();
        TestHandler testHandler = new TestHandler(testIt);
        AnnotationTestHandler annotationTestHandler = new AnnotationTestHandler(testIt);

        Testable stc = (Testable) Proxy.newProxyInstance(
                Testable.class.getClassLoader(),
                new Class[]{Testable.class},
                testHandler);

        Testable annotatedTestIt = (Testable) Proxy.newProxyInstance(
                AnnotationTestHandler.class.getClassLoader(),
                new Class[]{Testable.class},
                annotationTestHandler);

        testIt.doSomething("YTE");
        stc.doSomething("YTE");
        annotatedTestIt.doSomething("YTE");

    }
}
