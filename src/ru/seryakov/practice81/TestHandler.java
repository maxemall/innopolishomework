package ru.seryakov.practice81;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class TestHandler implements InvocationHandler {
    private Testable testable;

    public TestHandler(Testable testable) {
        this.testable = testable;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Имя метода " + method.getName());
        for (Parameter parameter : method.getParameters()) {
            System.out.println("Параметр " + parameter.getName());
        }
        return method.invoke(testable, args);
    }
}
