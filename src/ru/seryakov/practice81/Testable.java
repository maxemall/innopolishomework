package ru.seryakov.practice81;

public interface Testable {
    void doSomething(String some);

    void  doSomethingElse(String some, String someElse);
}
