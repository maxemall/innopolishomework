package ru.seryakov.practice81;

import ru.seryakov.practice81.practice83.Logged;

@Logged
public class TestClass implements Testable {

    @Override
    @Logged
    public void doSomething(String some) {
        System.out.println("It something " + some);
    }

    @Override
    public void  doSomethingElse(String some, String someElse) {
        System.out.println("It something " + some + " and it else " + someElse);
    }
}
