package ru.seryakov.testWeek8Json;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Student {
    private Integer id;
    private String city;
    private String fio;
    private Group group;

    public Student(Integer id, String city, String fio, Group group) {
        this.id = id;
        this.city = city;
        this.fio = fio;
        this.group = group;
    }
}
