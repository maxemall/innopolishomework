package ru.seryakov.testWeek8Json;

import java.io.IOException;
import java.util.logging.Logger;

public class Main {
    static Group group = new Group(1, "test group", 2019);
    static Student student = new Student(1, "Moscow", "Seryakoff MS", group);
    static Student student2 = new Student(2,"Moscow", "Ivanoff AA", group);
    static Student student3 = new Student(3, "DefaultCity", "Smirnoff VV", group);
    static final String PATH_TO_FILE = "/Users/maksim.seryakov/IdeaProjects/innopolishomework/src/ru/seryakov/task_week8_json/ru/seryakov/testWeek8Json/";

    public static void main(String[] args) throws IOException {
        Logger log = Logger.getLogger(Main.class.getName());
        FileServiceFactory fileServiceFactory = new FileServiceFactory(PATH_TO_FILE);
        StudentService studentService = new StudentService(fileServiceFactory);
        studentService.save(student);

        StudentService studentService1 = new StudentService(fileServiceFactory);
        studentService1.save(student2);

        fileServiceFactory = new FileServiceFactory(PATH_TO_FILE + "group-1/");
        StudentService studentService2 = new StudentService(fileServiceFactory);
        log.info(studentService2.get(2).toString());

    }
}

