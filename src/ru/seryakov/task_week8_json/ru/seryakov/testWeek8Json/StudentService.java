package ru.seryakov.testWeek8Json;

import com.google.gson.Gson;

import java.io.IOException;

public class StudentService {
    private final Gson gson = new Gson();
    private FileServiceFactory fileServiceFactory;

    public StudentService(FileServiceFactory fileServiceFactory) {
        this.fileServiceFactory = fileServiceFactory;
    }

    public void save(Student student) throws IOException {
        try {
            fileServiceFactory.getFileService(student).write(gson.toJson(student));
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public Student get(Integer studentId) throws IOException {
        try {
            return gson.fromJson(fileServiceFactory.getFileService(studentId).read(), Student.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
