package ru.seryakov.testWeek8Json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    private static final String STUDENT_JSON = "{\"id\":2,\"city\":\"Moscow\",\"fio\":\"Ivanoff AA\",\"group\":{\"id\":1,\"name\":\"test group" +
            "\",\"graduationYear\":2019}}";
    Student student;

    @Mock
    FileServiceFactory fileServiceFactory;

    @Mock
    FileService fileService;

    @InjectMocks
    StudentService subject;


    @Before
    public void setUp() throws FileNotFoundException {
        Group group = new Group(1, "test group", 2019);
        student = new Student(2, "Moscow", "Ivanoff AA", group);
        when(fileServiceFactory.getFileService(any(Student.class))).thenReturn(fileService);
        when(fileServiceFactory.getFileService(anyInt())).thenReturn(fileService);
    }

    @Test
    public void saveSuccess() throws IOException {
        subject.save(student);
    }

    @Test(expected = IOException.class)
    public void saveError() throws IOException {
        doThrow(new IOException()).when(fileService).write(anyString());
        subject.save(student);
    }

    @Test
    public void get() throws IOException {
        when(fileService.read()).thenReturn(STUDENT_JSON);
        assertEquals(subject.get(2), student);
    }

    @Test(expected = IOException.class)
    public void getError() throws IOException {
        doThrow(new IOException()).when(fileService).read();
        subject.get(2);
    }
}