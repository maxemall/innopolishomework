package ru.seryakov.testWeek8Json;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Group {
    private Integer id;
    private String name;
    private Integer graduationYear;

    public Group(Integer id, String name, Integer graduationYear) {
        this.id = id;
        this.name = name;
        this.graduationYear = graduationYear;
    }
}
