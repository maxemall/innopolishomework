package ru.seryakov.testWeek8Json;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileService {

    RandomAccessFile aFile;
    FileChannel channel;
    ByteBuffer buffer;

    public FileService(String pathToFile, Integer bufferSize) throws FileNotFoundException {
        aFile  = new RandomAccessFile(pathToFile, "rw");
        channel = aFile.getChannel();
        buffer = ByteBuffer.allocate(bufferSize);
    }

    public void write(String newData) throws IOException {
        buffer.clear();
        buffer.put(newData.getBytes());
        buffer.flip();
        while (buffer.hasRemaining()) {
            channel.write(buffer);
        }
    }

    public String read() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        int bytesRead = channel.read(buffer);
        while (bytesRead != -1) {
            buffer.flip();
            while (buffer.hasRemaining()) {
                stringBuilder.append((char) buffer.get());
            }
            buffer.clear();
            bytesRead = channel.read(buffer);
        }
        return stringBuilder.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        aFile.close();
    }
}
