package ru.seryakov.testWeek8Json;

import java.io.FileNotFoundException;


public class FileServiceFactory {
    private static final String FILE_NAME = "%sgroup-%s/student-%s.sv";
    private static final String FILE_NAME_SINGLE = "%sstudent-%s.sv";
    private String pathToFile;

    public FileServiceFactory(String pathToFile) {
        this.pathToFile = pathToFile;
    }


    public FileService getFileService(Student student) throws FileNotFoundException {
        return new FileService(String.format(FILE_NAME, pathToFile, student.getGroup().getId(), student.getId()), 512);
    }

    public FileService getFileService(Integer studentId) throws FileNotFoundException {
        return new FileService(String.format(FILE_NAME_SINGLE, pathToFile, studentId), 512);
    }
}
