package ru.seryakov.task31;

import java.io.IOException;

public class ExceptionsExample {

    static String str = "Hello world";
    static String npe;

    public static void main(String[] args) {
        System.out.println(str);
        try {
            getNullPointer();
        } catch (NullPointerException e) {
            System.out.println("NullPointer Exception done");
        }

        try {
            getOutOfBound();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("IndexOutOfBoundsException done");
        }

        try {
            getIOException();
        } catch (IOException e) {
            System.out.println("IOException done");
        }

    }

    private static void getNullPointer() {
        System.out.println(npe.length());
    }

    private static char getOutOfBound() {
        return str.toCharArray()[str.length()];
    }

    private static void getIOException() throws IOException {
        throw new IOException();
    }


}
