package ru.seryakov.practice82;

import ru.seryakov.practice81.TestClass;

public class Main {
    public static void main(String[] args) {
        ClassAnalyzer classAnalyzer = new ClassAnalyzer(TestClass.class);
        classAnalyzer.analyze();
    }
}
