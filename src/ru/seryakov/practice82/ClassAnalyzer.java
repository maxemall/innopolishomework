package ru.seryakov.practice82;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

public class ClassAnalyzer {
    private Map<String, List> classDetails = new HashMap<>();
    private Map<String, List> methodDetails = new HashMap<>();
    private Class clazz;

    public ClassAnalyzer(Class clazz) {
        this.clazz = clazz;
    }

    public void analyze() {
        getClassInfo();
        getMetodsInfo();
        printClassInfo();
        printMethodsInfo();
    }

    private void printClassInfo() {
        for (String s : classDetails.keySet()) {
            System.out.println("Класс: " + s);
            for (Object o : classDetails.get(s)) {
                System.out.println("Аннотоация: " +  ((Annotation) o).annotationType().getSimpleName());
            }

        }
    }

    private void printMethodsInfo() {
        for (String s : methodDetails.keySet()) {
            System.out.println("Метод: " + s);
            for (Object o : methodDetails.get(s)) {
                System.out.println("Аннотоация: " +  ((Annotation) o).annotationType().getSimpleName());
            }
        }
    }

    private void getClassInfo() {
        classDetails.put(clazz.getSimpleName(), Arrays.asList(clazz.getAnnotations()));
    }

    private void getMetodsInfo() {
        for (Method method : clazz.getDeclaredMethods()) {
                methodDetails.put(method.getName(), Arrays.asList(method.getAnnotations()));
            }
        }
    }

