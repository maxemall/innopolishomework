package ru.seryakov.task151.task2;


import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Задание 2 (пакет 2)
 * <p>
 * Написать программу, которая выполняет сохранение в файл и чтение из файла простого (содержащего только
 * поля-примитивы и String) объекта. Не использовать для этого ObjectOutput и ObjectInput (реализовать
 * механизм сериализации-десериализации самостоятельно). Скорее всего, вам пригодится рефлексия (reflections API).
 */
public class ObjectReadWrite {
    private final String NO_FILE_FOUND = "%s file not found";
    private final String NO_CLASS_FOUND = "%s class not found";
    private final String NO_CONSTRUCTOR_FOUND = "All args constructor for class %s not found";
    private final String INIT_ERROR = "Can't initialize instance of %s class";
    private final String ACCESS_ERROR = "Check that %s class is not contains private fields";
    private final String TYPE_NOT_SUPPORT = "Type %s is not supported to use";



    private Map<String, String> objectMap = new LinkedHashMap<>();
    private Map<Class, Object> classMap = new LinkedHashMap<>();
    private Logger log = Logger.getLogger(this.getClass().getName());
    private String objectClassName;

    private void loadObject(String fileName) throws FileNotFoundException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            objectClassName = scanner.nextLine();
            while (scanner.hasNext()) {
                Class fieldClass = Class.forName(scanner.nextLine());
                checkType(fieldClass);
                Object field;
                if (fieldClass == Character.class) {
                    field = scanner.nextLine().charAt(0);
                } else {
                    Constructor constructor;
                    try {
                        constructor = fieldClass.getConstructor(String.class);
                    } catch (NoSuchMethodException e) {
                        log.severe(String.format(NO_CONSTRUCTOR_FOUND, fieldClass));
                        e.printStackTrace();
                        throw e;
                    }
                    try {
                        field = constructor.newInstance(scanner.nextLine());
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        log.severe(String.format(INIT_ERROR, fieldClass));
                        e.printStackTrace();
                        throw e;
                    }
                }
                classMap.put(fieldClass, field);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.severe(String.format(NO_FILE_FOUND, fileName));
            throw e;
        } catch (ClassNotFoundException e) {
            log.severe(String.format(NO_CLASS_FOUND, objectClassName));
            throw e;
        }
    }

    private Object mapToObject() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Object result;
        try {
            Class clazz = Class.forName(objectClassName);

            Constructor constructor;
            try {
                constructor = clazz.getConstructor(classMap.keySet().toArray(new Class[classMap.size()]));
            } catch (NoSuchMethodException e) {
                log.severe(String.format(NO_CONSTRUCTOR_FOUND, clazz));
                e.printStackTrace();
                throw e;
            }
            try {
                result = constructor.newInstance(classMap.values().toArray());
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                log.severe(String.format(INIT_ERROR, clazz));
                e.printStackTrace();
                throw e;
            }
        } catch (ClassNotFoundException e) {
            log.severe(String.format(NO_CLASS_FOUND, objectClassName));
            throw e;
        }
        return result;
    }


    public Object deserialize(String fileName) throws FileNotFoundException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        loadObject(fileName);
        return mapToObject();
    }

    public void serialize(Object o, String fileName) throws IllegalAccessException, FileNotFoundException {
        objectToMap(o);
        saveObject(fileName);
    }

    private void objectToMap(Object o) throws IllegalAccessException {
        objectClassName = o.getClass().getName();
        for (Field declaredField : o.getClass().getDeclaredFields()) {
            try {
                checkType(declaredField.getType());
                objectMap.put(declaredField.getType().getName(), declaredField.get(o).toString());
            } catch (IllegalAccessException e) {
                log.severe(String.format(ACCESS_ERROR, objectClassName));
                e.printStackTrace();
                throw e;
            }
        }
    }

    private void saveObject(String fileName) throws FileNotFoundException {
        try (PrintWriter printWriter = new PrintWriter(fileName)) {
            printWriter.println(objectClassName);
            for (String s : objectMap.keySet()) {
                printWriter.println(s);
                printWriter.println(objectMap.get(s));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.severe(String.format(NO_FILE_FOUND, fileName));
            throw e;
        }
    }

    private void checkType(Class clazz) throws IllegalArgumentException {
        try {
            AllowedTypes.valueOf(clazz.getSimpleName());
        } catch (IllegalArgumentException e) {
            log.severe( String.format(TYPE_NOT_SUPPORT, clazz.getSimpleName()));
            throw e;
        }
    }
}
