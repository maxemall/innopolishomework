package ru.seryakov.task151.task2;

import java.util.Objects;

public class TestClass {

    private Integer intField;
    private Long longField;
    private Float floatField;
    private Double doubleField;
    private Short shortField;
    private Byte byteField;
    private Character charField;
    private Boolean booleanField;
    private String stringField;


    public TestClass(Integer intField, Long longField, Float floatField, Double doubleField, Short shortField, Byte byteField, Character charField, Boolean booleanField, String stringField) {
        this.intField = intField;
        this.longField = longField;
        this.floatField = floatField;
        this.doubleField = doubleField;
        this.shortField = shortField;
        this.byteField = byteField;
        this.charField = charField;
        this.booleanField = booleanField;
        this.stringField = stringField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestClass testClass = (TestClass) o;
        return Objects.equals(intField, testClass.intField) &&
                Objects.equals(longField, testClass.longField) &&
                Objects.equals(floatField, testClass.floatField) &&
                Objects.equals(doubleField, testClass.doubleField) &&
                Objects.equals(shortField, testClass.shortField) &&
                Objects.equals(byteField, testClass.byteField) &&
                Objects.equals(charField, testClass.charField) &&
                Objects.equals(booleanField, testClass.booleanField) &&
                Objects.equals(stringField, testClass.stringField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intField, longField, floatField, doubleField, shortField, byteField, charField, booleanField, stringField);
    }
}
