package ru.seryakov.task151.task2;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;


/**
 * Задание 2 (пакет 2)
 * <p>
 * Написать программу, которая выполняет сохранение в файл и чтение из файла простого (содержащего только
 * поля-примитивы и String) объекта. Не использовать для этого ObjectOutput и ObjectInput (реализовать
 * механизм сериализации-десериализации самостоятельно). Скорее всего, вам пригодится рефлексия (reflections API).
 */
public class Main {

    static final String FILE_NAME = "./src/ru/seryakov/task151/task2/resource/outFile";
    static TestClass testObject = new TestClass(1, 2L, 3.0f, 4.0, (short)5,
            (byte)6, 'a', true, "Something");

    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {
        ObjectReadWrite objectReadWrite = new ObjectReadWrite();
        objectReadWrite.serialize(testObject, FILE_NAME);
        Object o = objectReadWrite.deserialize(FILE_NAME);
        System.out.println(testObject.equals(o));
    }
}
