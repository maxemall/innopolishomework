package ru.seryakov.task151.task1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;


/**
 * Задание 1 (пакет 1)
 *
 * Написать программу, читающую текстовый файл. Программа должна составлять отсортированный по алфавиту список слов,
 * найденных в файле и сохранять его в файл-результат. Найденные слова не должны повторяться. Оптимально приводить их
 * к одному регистру. Одно слово в разных падежах это разные слова.
 */
public class FileSorter {
    private final String FILE_NAME = "./src/ru/seryakov/task151/task1/resource/textFile";
    private final String FILE_NAME2 = "./src/ru/seryakov/task151/task1/resource/outFile";
    private final Logger log = Logger.getLogger(FileSorter.class.getName());
    private List<String> stringList = new ArrayList<>();

    public void sortFile() {
        readFile();
        writeFile();
    }

    private void readFile() {

        try (Scanner scanner = new Scanner(new File(FILE_NAME))){
            while (scanner.hasNext()) {
                stringList.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        stringList.sort(String::compareToIgnoreCase);
        stringList.stream().map(s -> "Line: " + s).forEach(log::info);
    }

    private void writeFile() {
        try (PrintWriter printWriter = new PrintWriter(FILE_NAME2)){
            stringList.forEach(printWriter::println);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



    }
}
