package ru.seryakov.task151.task3;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.logging.Logger;


/**
 * Задание 3 (пакет 3)
 *
 * Создать программу, при запуске двух экземпляров которой (возможно, с разными параметрами) эти экземпляры будут
 * обмениваться сообщениями. Обеспечить асинхронную передачу сообщений между клиентом и сервером (любой из них может
 * в любой момент отправить сообщение другому).
 */
public class ClientServer {
    public static final int PORT = 8080;
    private static final Logger log = Logger.getLogger(ClientServer.class.getName());
    private static final String START_LSNR_INFO = "Listener started";
    private static final String START_SNDR_INFO = "Sender started";
    private static final String START_INFO = "ClientServer is running...";
    private final String START_CLNT_INFO = "%s client started";


    public static void main(String[] args) throws IOException {
        ClientServer clientServer = new ClientServer();
        if (args.length > 0 && "-clientServer".equals(args[0])) {
            clientServer.startServer();
        } else {
            clientServer.startClient();
        }
    }

    public void startServer() throws IOException {
        log.info(START_INFO);

        try(ServerSocket sSocket = new ServerSocket(PORT);
            Socket socket = sSocket.accept()
        ) {

            runListener(socket);
            runSender(socket);

            while(true) {
                //...
            }

        }
    }

    public void startClient() throws IOException {
        InetAddress addr = InetAddress.getByName("127.0.0.1");
        try (Socket socket = new Socket(addr, ClientServer.PORT)) {
            runListener(socket);
            runSender(socket);

            log.info(String.format(START_CLNT_INFO, addr.toString()));

            while(true) {
                //...
            }
        }


    }

    public static void runListener(Socket socket) {
        Executors.newSingleThreadExecutor().execute(() -> {
            log.info(START_LSNR_INFO);
            String inputStr = "";
            BufferedReader in = null;
            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    inputStr = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!inputStr.isEmpty()) {
                    System.out.println(inputStr);
                }
            }
        });
    }

    public static void runSender(Socket socket) {
        Executors.newSingleThreadExecutor().execute(() -> {
            PrintWriter out = null;
            try {
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info(START_SNDR_INFO);
                String outputStr;
                while (true) {
                    outputStr = new Scanner(System.in).next();
                    out.println(outputStr);
                }
        });
    }
}
