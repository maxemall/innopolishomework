<%--
  Created by IntelliJ IDEA.
  User: maksim.seryakov
  Date: 2019-02-09
  Time: 12:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Groups</title>
</head>
<body>
<c:forEach var="group" items="${list}">
    <p><a href = 'students?groupId=${group["id"]}'> ${group.groupName}</a></p>
</c:forEach>
<br>
<a href="login">back</a>
</body>
</html>
