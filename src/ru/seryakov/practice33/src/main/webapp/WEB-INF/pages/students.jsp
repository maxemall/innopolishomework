<%--
  Created by IntelliJ IDEA.
  User: maksim.seryakov
  Date: 2019-02-09
  Time: 12:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Students</title>
</head>
<body>
<c:forEach var="student" items="${studentList}">
    <p><a href = 'lessons?studentId=${student["id"]}'> ${student.firstName} ${student.lastName}</a></p>
</c:forEach>
<br>
<a href="groups">back</a>
</body>
</html>
