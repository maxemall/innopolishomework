<%--
  Created by IntelliJ IDEA.
  User: m.biryukov
  Date: 25.03.2019
  Time: 20:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form action="/j_username_security_check" method="post">
    <h2>Log in</h2>
    <input type="text" name="j_username" placeholder="Input your login" required><br>
    <input type="password" name="j_password" placeholder="Input your password" required><br>
    <input type="submit" value="Login">
    <c:if test="${not empty loginError}">
        <H3 style="color:#CC0000">
            Неверное имя пользователя, или пароль
        </H3>
    </c:if>
</form>
</body>
</html>
