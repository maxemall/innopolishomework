package seryakov.practice33.da;

import org.springframework.stereotype.Component;
import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.mapper.GradeMapper;
import seryakov.practice33.model.Grade;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class GradeDaoImpl extends AbstractDao<Grade, Integer> {


    public GradeDaoImpl(ConnectionManagerJdbc connectionManagerJdbc) {
        super(connectionManagerJdbc);
    }

    @Override
    protected void initializeModelMapper() {
        modelMapper = new GradeMapper();
    }

    @Override
    protected String getInsertSql() {
        return "INSERT  INTO grade " +
                "(student_id, lesson_id, grade) " +
                "VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateSql() {
        return "UPDATE grade SET student_id = ?, lesson_id = ?, grade = ? " +
                "WHERE  id = ?";
    }

    @Override
    protected void setInsertPreparedStatementParams(PreparedStatement preparedStatement, Grade grade) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, grade);
    }

    @Override
    protected void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, Grade grade) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, grade);
        int i = preparedStatement.getParameterMetaData().getParameterCount();
        preparedStatement.setInt(++i, grade.getId());

    }

    private void setSimilarPreparedStatementParams(PreparedStatement preparedStatement, Grade grade) throws SQLException {
        int i = 1;
        preparedStatement.setInt(i++, grade.getStudentId());
        preparedStatement.setInt(i++, grade.getLessonId());
        preparedStatement.setInt(i, grade.getGrade());
    }

    @Override
    protected String getTableName() {
        return "grade";
    }
}
