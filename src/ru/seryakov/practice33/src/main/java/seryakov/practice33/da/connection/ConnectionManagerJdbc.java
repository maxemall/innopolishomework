package seryakov.practice33.da.connection;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class ConnectionManagerJdbc {
    private Connection[] connectionPool;

    public ConnectionManagerJdbc() {
        this.connectionPool = new Connection[5];
    }

    public MyConnection getConnection() throws SQLException {

        MyConnection resultConnection = null;
        do {
            Connection localConnection;
            for (int i = 0; i <= connectionPool.length - 1; i++) {
                localConnection = connectionPool[i];
                if (localConnection == null || localConnection.isClosed()) {
                    try {
                            Class.forName("org.postgresql.Driver");
                        connectionPool[i] = DriverManager.getConnection(
                                "jdbc:postgresql://localhost:5432/postgresInno?currentSchema=public",
                                "postgres", System.getProperty("jdbcPassword"));
                        localConnection = connectionPool[i];
                        resultConnection = new ConnectionWrapper(connectionPool[i]);
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        } while (resultConnection == null || resultConnection.isClosed());
        return resultConnection;
    }
}
