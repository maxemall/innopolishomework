package seryakov.practice33.service.mapper;

import seryakov.practice33.model.Group;
import seryakov.practice33.service.dto.GroupDto;
import java.util.ArrayList;
import java.util.List;

public class GroupDtoConverter {

    private GroupDtoConverter() {
    }

    public static GroupDto convertGroup2Dto(Group group) {
        return new GroupDto().setId(group.getId()).setGroupName(group.getGroupName()).setGroupGraduating(group.getGroupGraduating());
    }

    public static List<GroupDto> convertGroups2DtoList(List<Group> groups) {
        List<GroupDto> groupDtos = new ArrayList<>();
        for (Group group : groups) {
            groupDtos.add(convertGroup2Dto(group));
        }
        return groupDtos;
    }

    public static Group convertDto2Group(GroupDto groupDto) {
        return new Group().setId(groupDto.getId()).setGroupName(groupDto.getGroupName()).setGroupGraduating(groupDto.getGroupGraduating());
    }



}
