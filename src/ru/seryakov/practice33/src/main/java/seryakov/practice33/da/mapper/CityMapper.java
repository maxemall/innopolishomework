package seryakov.practice33.da.mapper;


import seryakov.practice33.model.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityMapper implements ModelMapper<City> {

    public City mapResultSetToModel(ResultSet resultSet) throws SQLException {
        City city = new City();
            city.setId(resultSet.getInt("id")).setCityName(resultSet.getString("name"));
        return city;
    }

}
