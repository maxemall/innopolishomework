package seryakov.practice33.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class StudentDto {
    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthDate;
    private Integer groupId;
    private Integer cityId;
}
