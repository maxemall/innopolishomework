package seryakov.practice33.service.mapper;

import seryakov.practice33.model.Student;
import seryakov.practice33.service.dto.StudentDto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class StudentDtoConverter {

    private StudentDtoConverter() {
    }

    public static StudentDto convertStudent2Dto(Student student) {
        return new StudentDto(student.getId(), student.getFirstName(), student.getMiddleName(),student.getLastName(),
                student.getBirthDate(), student.getGroupId(), student.getCityId());
    }

    public static Student convertDto2Student(StudentDto studentDto) {
        return new Student().setId(studentDto.getId()).setFirstName(studentDto.getFirstName())
                .setLastName(studentDto.getLastName())
                .setMiddleName(studentDto.getMiddleName())
                .setBirthDate(new Date(studentDto.getBirthDate().getTime()))
                .setGroupId(studentDto.getGroupId())
                .setCityId(studentDto.getCityId());
    }

    public static List<StudentDto> convertStudents2DtoList(List<Student> studentList) {
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : studentList) {
            studentDtoList.add(convertStudent2Dto(student));
        }
        return studentDtoList;
    }


}
