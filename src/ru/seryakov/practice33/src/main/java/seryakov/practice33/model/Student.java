package seryakov.practice33.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class Student {
    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthDate;
    private Integer groupId;
    private Integer cityId;
}
