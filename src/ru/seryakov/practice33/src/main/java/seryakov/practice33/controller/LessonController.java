package seryakov.practice33.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import seryakov.practice33.service.LessonService;

@RestController
public class LessonController {

    @Autowired
    LessonService lessonService;

    @RequestMapping("/lessons")
    public String getStudents(@RequestParam("studentId") Integer id,
            Model model) {
        model.addAttribute("lessonList", lessonService.findByStudent(id));
        return "lessons";
    }

}
