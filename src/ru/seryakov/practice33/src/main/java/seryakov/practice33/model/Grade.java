package seryakov.practice33.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Grade {
    private Integer id;
    private Integer studentId;
    private Integer lessonId;
    private Integer grade;
}
