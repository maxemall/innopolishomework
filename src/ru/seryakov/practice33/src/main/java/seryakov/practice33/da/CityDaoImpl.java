package seryakov.practice33.da;

import org.springframework.stereotype.Component;
import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.mapper.CityMapper;
import seryakov.practice33.model.City;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class CityDaoImpl extends AbstractDao<City, Integer> {


    public CityDaoImpl(ConnectionManagerJdbc connectionManagerJdbc) {
        super(connectionManagerJdbc);
    }

    @Override
    protected void initializeModelMapper() {
        modelMapper = new CityMapper();
    }

    @Override
    protected String getInsertSql() {
        return "INSERT  INTO city " +
                "(name) " +
                "VALUES (?)";
    }

    @Override
    protected String getUpdateSql() {
        return "UPDATE group SET name = ? WHERE  id = ?";
    }

    @Override
    protected void setInsertPreparedStatementParams(PreparedStatement preparedStatement, City city) throws SQLException {
        int i = 1;
        preparedStatement.setString(i, city.getCityName());
    }

    @Override
    protected void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, City city) throws SQLException {
        int i = 1;
        preparedStatement.setString(i++, city.getCityName());
        preparedStatement.setInt(i, city.getId());
    }

    @Override
    protected String getTableName() {
        return "city";
    }
}
