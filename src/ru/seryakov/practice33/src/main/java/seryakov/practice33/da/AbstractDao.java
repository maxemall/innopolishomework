package seryakov.practice33.da;

import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.connection.MyConnection;
import seryakov.practice33.da.mapper.ModelMapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public abstract class AbstractDao<T, I> implements Dao<T, I> {

    private static final Logger log = Logger.getLogger(AbstractDao.class.getName());
    private static final String SELECT_FROM = "SELECT * FROM ";
    private static final String WHERE_ID = " WHERE  id = ?";
    public static final String DATABASE_ERROR = "Database error ";
    private ConnectionManagerJdbc connectionManagerJdbc;
    protected ModelMapper<T> modelMapper;

    public AbstractDao(ConnectionManagerJdbc connectionManagerJdbc) {
        this.connectionManagerJdbc = connectionManagerJdbc;
        initializeModelMapper();
    }

    protected abstract void initializeModelMapper();

    protected String getFindByIdSql() {
        StringBuilder stringBuilder = new StringBuilder(SELECT_FROM);
        stringBuilder.append(getTableName());
        stringBuilder.append(WHERE_ID);
        return stringBuilder.toString();
    }

    protected String getFindByFieldSql(String fieldName) {
        StringBuilder stringBuilder = new StringBuilder(SELECT_FROM);
        stringBuilder.append(getTableName());
        stringBuilder.append(" WHERE ");
        stringBuilder.append(fieldName);
        stringBuilder.append(" = ?");
        return stringBuilder.toString();
    }

    protected String getFindAllSql() {
        StringBuilder stringBuilder = new StringBuilder(SELECT_FROM);
        stringBuilder.append(getTableName());
        return stringBuilder.toString();
    }

    protected String getDeleteSql() {
        StringBuilder stringBuilder = new StringBuilder("DELETE FROM ");
        stringBuilder.append(getTableName());
        stringBuilder.append(WHERE_ID);
        return stringBuilder.toString();
    }

    protected abstract String getInsertSql();

    protected abstract String getUpdateSql();


    protected abstract void setInsertPreparedStatementParams(PreparedStatement preparedStatement, T t) throws SQLException;

    protected abstract void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, T t) throws SQLException;

    protected void setDomainId(ResultSet resultSet, T t) throws SQLException {
        Method method = null;
        try {
            Class clazz = t.getClass();
            method = clazz.getMethod("setId", Integer.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("You must define setInt in class " + t.getClass().getName(), e.getCause());
        }
        try {
            method.invoke(t, resultSet.getInt(1));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Can't access setInt in class " + t.getClass().getName(), e.getCause());
        }
    }

    protected abstract String getTableName();

    public T findById(I id) {
        T result = null;

        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            try (PreparedStatement pstm = connection.prepareStatement(getFindByIdSql())) {

                pstm.setInt(1, (Integer) id);
                try (ResultSet resultSet = pstm.executeQuery()) {
                    if (resultSet.next())
                        result = modelMapper.mapResultSetToModel(resultSet);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(DATABASE_ERROR, e);
        }

        return result;
    }

    public List<T> findByField(String fieldName, I id) {
        List<T> result = new ArrayList<>();
        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            try (PreparedStatement pstm = connection.prepareStatement(getFindByFieldSql(fieldName))) {
                pstm.setInt(1, (Integer) id);
                try (ResultSet resultSet = pstm.executeQuery()) {
                    while (resultSet.next()) {
                        result.add(modelMapper.mapResultSetToModel(resultSet));
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(DATABASE_ERROR + getFindByFieldSql(fieldName), e);
        }
        return result;
    }

    @Override
    public List<T> findAll() {
        List<T> result = new ArrayList<>();
        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            try (Statement stm = connection.createStatement()) {
                try (ResultSet resultSet = stm.executeQuery(getFindAllSql())) {
                    while (resultSet.next()) {
                        result.add(modelMapper.mapResultSetToModel(resultSet));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(DATABASE_ERROR, e);
        }
        return result;
    }

    @Override
    public T insert(T t) {
        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            try (PreparedStatement pstm = connection.prepareStatement(getInsertSql(), Statement.RETURN_GENERATED_KEYS)) {
                setInsertPreparedStatementParams(pstm, t);

                if (pstm.executeUpdate() == 0)
                    throw new RuntimeException("Insert into table" + getTableName() + " is fail");

                try (ResultSet resultSet = pstm.getGeneratedKeys()) {
                    if (resultSet.next()) setDomainId(resultSet, t);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(DATABASE_ERROR, e);
        }
        return t;
    }

    @Override
    public T update(T t) {
        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            PreparedStatement pstm =
                    connection.prepareStatement(getUpdateSql());
            setUpdatePreparedStatementParams(pstm, t);

            if (pstm.executeUpdate() == 0)
                throw new RuntimeException("Update table " + getTableName() + " is fail");

        } catch (SQLException e) {
            throw new RuntimeException(DATABASE_ERROR, e);
        }
        return t;
    }

    @Override
    public boolean delete(I id) {
        boolean result;
        try (MyConnection connection = connectionManagerJdbc.getConnection()) {
            try (PreparedStatement pstm = connection.prepareStatement(getDeleteSql())) {
                pstm.setInt(1, (Integer) id);

                if (pstm.executeUpdate() == 0)
                    result = false;
                else result = true;

            }
        } catch (SQLException e) {
            throw new RuntimeException("Database error", e);
        }
        return result;
    }
}
