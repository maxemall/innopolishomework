package seryakov.practice33.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GroupDto {
    private Integer id;
    private String groupName;
    private String groupGraduating;
}
