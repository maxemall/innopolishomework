package seryakov.practice33.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seryakov.practice33.da.GradeDaoImpl;
import seryakov.practice33.service.dto.GradeDto;
import seryakov.practice33.service.mapper.GradeDtoConverter;

import java.util.List;

@Service
public class GradeService {

    @Autowired
    private GradeDaoImpl gradeDao;

    public GradeService(GradeDaoImpl gradeDao) {
        this.gradeDao = gradeDao;
    }

    public List<GradeDto> findAll() {
        return GradeDtoConverter.convertGrades2DtoList(gradeDao.findAll());
    }

    public GradeDto getGrade(Integer id) {
        return GradeDtoConverter.convertGrade2Dto(gradeDao.findById(id));
    }

    public GradeDto updateGrade(GradeDto gradeDto) {
        return GradeDtoConverter.convertGrade2Dto(gradeDao.update(GradeDtoConverter.convertDto2Grade(gradeDto)));
    }

    public GradeDto insertGrade(GradeDto gradeDto) {
        return GradeDtoConverter.convertGrade2Dto(gradeDao.insert(GradeDtoConverter.convertDto2Grade(gradeDto)));
    }

    public Boolean deleteGrade(Integer id) {
        return gradeDao.delete(id);
    }

    public List<GradeDto> findByStudent(Integer studentId) {
        return GradeDtoConverter.convertGrades2DtoList(gradeDao.findByField("student_id", studentId));
    }

    public List<GradeDto> findByLesson(Integer lessonId) {
        return GradeDtoConverter.convertGrades2DtoList(gradeDao.findByField("lesson_id", lessonId));
    }

}
