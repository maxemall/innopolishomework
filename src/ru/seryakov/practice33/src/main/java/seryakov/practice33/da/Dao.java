package seryakov.practice33.da;

import java.util.List;

public interface Dao<T, I> {
    T findById(I id);

    List<T> findAll();

    T insert(T t);

    T update(T t);

    boolean delete(I id);
}

