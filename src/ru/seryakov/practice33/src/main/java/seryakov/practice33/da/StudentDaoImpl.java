package seryakov.practice33.da;

import org.springframework.stereotype.Component;
import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.mapper.StudentMapper;
import seryakov.practice33.model.Student;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class StudentDaoImpl extends seryakov.practice33.da.AbstractDao<Student, Integer> {


    public StudentDaoImpl(ConnectionManagerJdbc connectionManagerJdbc) {
        super(connectionManagerJdbc);
    }

    @Override
    protected void initializeModelMapper() {
        modelMapper = new StudentMapper();
    }

    @Override
    protected String getInsertSql() {
        return "INSERT  INTO student " +
                "(first_name, last_name, middle_name, date_of_birth, group_id, city_id) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateSql() {
        return "UPDATE group SET first_name = ?, last_name = ?, middle_name = ?, date_of_birth = ?, group_id = ?, city_id = ? " +
                "WHERE  id = ?";
    }

    @Override
    protected void setInsertPreparedStatementParams(PreparedStatement preparedStatement, Student student) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, student);
    }

    @Override
    protected void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, Student student) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, student);
        int i = preparedStatement.getParameterMetaData().getParameterCount();
        preparedStatement.setInt(++i, student.getId());

    }

    private void setSimilarPreparedStatementParams(PreparedStatement preparedStatement, Student student) throws SQLException {
        int i = 1;
        preparedStatement.setString(i++, student.getFirstName());
        preparedStatement.setString(i++, student.getLastName());
        preparedStatement.setString(i++, student.getMiddleName());
        preparedStatement.setDate(i++, new Date(student.getBirthDate().getTime()));
        preparedStatement.setInt(i++, student.getGroupId());
        preparedStatement.setInt(i, student.getCityId() == null ? 0 : student.getCityId());
    }

    @Override
    protected String getTableName() {
        return "student";
    }


}
