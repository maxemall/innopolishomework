package seryakov.practice33.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seryakov.practice33.da.GroupDaoImpl;
import seryakov.practice33.service.dto.GroupDto;
import seryakov.practice33.service.mapper.GroupDtoConverter;

import java.util.List;

@Service
public class GroupService {

    @Autowired
    private GroupDaoImpl groupDao;

    @Autowired
    private StudentService studentService;

    public GroupService(GroupDaoImpl groupDao) {
        this.groupDao = groupDao;
    }

    public List<GroupDto> findAll() {
        return GroupDtoConverter.convertGroups2DtoList(groupDao.findAll());
    }

    public GroupDto getGroup(Integer id) {
        return GroupDtoConverter.convertGroup2Dto(groupDao.findById(id));
    }

    public GroupDto updateGroup(GroupDto groupDto) {
        return GroupDtoConverter.convertGroup2Dto(groupDao.update(GroupDtoConverter.convertDto2Group(groupDto)));
    }

    public GroupDto insertGroup(GroupDto groupDto) {
        return GroupDtoConverter.convertGroup2Dto(groupDao.insert(GroupDtoConverter.convertDto2Group(groupDto)));
    }

    public Boolean deleteGroup(Integer id) {
        return groupDao.delete(id);
    }

    public GroupDto findByStudent(Integer studentId) {
        return GroupDtoConverter.convertGroup2Dto(groupDao.findById(studentService.getStudent(studentId).getGroupId()));
    }


}
