package seryakov.practice33.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seryakov.practice33.da.LessonDaoImpl;
import seryakov.practice33.service.dto.GroupDto;
import seryakov.practice33.service.dto.LessonDto;
import seryakov.practice33.service.mapper.LessonDtoConverter;
import java.util.List;

@Service
public class LessonService {

    @Autowired
    private LessonDaoImpl lessonDao;

    @Autowired
    private GroupService groupService;

    public LessonService(LessonDaoImpl lessonDao) {
        this.lessonDao = lessonDao;
    }

    public List<LessonDto> findAll() {
        return LessonDtoConverter.convertLessons2DtoList(lessonDao.findAll());
    }

    public LessonDto getLesson(Integer id) {
        return LessonDtoConverter.convertLesson2Dto(lessonDao.findById(id));
    }

    public LessonDto updateLesson(LessonDto lessonDto) {
        return LessonDtoConverter.convertLesson2Dto(lessonDao.update(LessonDtoConverter.convertDto2Lesson(lessonDto)));
    }

    public LessonDto insertLesson(LessonDto lessonDto) {
        return LessonDtoConverter.convertLesson2Dto(lessonDao.insert(LessonDtoConverter.convertDto2Lesson(lessonDto)));
    }

    public Boolean deleteLesson(Integer id) {
        return lessonDao.delete(id);
    }

    public List<LessonDto> findByGroup(Integer groupId) {
        return LessonDtoConverter.convertLessons2DtoList(lessonDao.findByField("group_id", groupId));
    }

    public List<LessonDto> findByStudent(Integer studentId) {
        GroupDto groupDto = groupService.findByStudent(studentId);
        return LessonDtoConverter.convertLessons2DtoList(lessonDao.findByField("group_id", groupDto.getId()));
    }
}
