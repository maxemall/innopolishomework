package seryakov.practice33.da.mapper;


import seryakov.practice33.model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentMapper implements ModelMapper<Student> {

    public Student mapResultSetToModel(ResultSet resultSet) throws SQLException {
        Student student = new Student();
            student.setId(resultSet.getInt("id")).setFirstName(resultSet.getString("first_name"))
                .setMiddleName(resultSet.getString("middle_name"))
                .setLastName(resultSet.getString("last_name"))
                .setBirthDate(resultSet.getDate("date_of_birth"))
                .setGroupId(resultSet.getInt("group_id"))
                .setCityId(resultSet.getInt("city_id"));
        return student;
    }

}
