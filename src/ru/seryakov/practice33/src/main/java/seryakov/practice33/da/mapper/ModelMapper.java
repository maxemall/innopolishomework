package seryakov.practice33.da.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ModelMapper<T> {
    public T mapResultSetToModel(ResultSet resultSet) throws SQLException;
}
