package seryakov.practice33.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GradeDto {
    private Integer id;
    private Integer studentId;
    private Integer lessonId;
    private Integer grade;
}
