package seryakov.practice33.da.connection;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public interface MyConnection extends AutoCloseable {
    Statement createStatement() throws SQLException;
    PreparedStatement prepareStatement(String sql)
            throws SQLException;
    PreparedStatement prepareStatement(String sql, Integer integer)
            throws SQLException;
    boolean isClosed() throws SQLException;
    void close() throws SQLException;
}
