package seryakov.practice33.da.mapper;


import seryakov.practice33.model.Group;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper implements ModelMapper<Group> {

    public Group mapResultSetToModel(ResultSet resultSet) throws SQLException {
        Group group = new Group();
            group.setId(resultSet.getInt("id")).setGroupName(resultSet.getString("group_name"))
                    .setGroupGraduating(resultSet.getString("group_graduating"));
        return group;
    }

}
