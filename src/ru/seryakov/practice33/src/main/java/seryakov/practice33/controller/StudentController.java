package seryakov.practice33.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import seryakov.practice33.service.StudentService;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @RequestMapping("/students")
    public String getStudents(@RequestParam("groupId") Integer id,
            Model model) {
        model.addAttribute("studentList", studentService.findByGroup(id));
        return "students";
    }

}
