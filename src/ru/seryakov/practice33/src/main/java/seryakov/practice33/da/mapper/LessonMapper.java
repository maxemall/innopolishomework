package seryakov.practice33.da.mapper;


import seryakov.practice33.model.Lesson;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LessonMapper implements ModelMapper<Lesson> {

    public Lesson mapResultSetToModel(ResultSet resultSet) throws SQLException {
        Lesson lesson = new Lesson();
            lesson.setId(resultSet.getInt("id")).setClassRoom(resultSet.getString("class_room"))
                .setTopic(resultSet.getString("topic"))
                .setDate(resultSet.getDate("date"))
                .setGroupId(resultSet.getInt("group_id"));
        return lesson;
    }

}
