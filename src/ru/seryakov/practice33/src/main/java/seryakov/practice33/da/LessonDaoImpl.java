package seryakov.practice33.da;

import org.springframework.stereotype.Component;
import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.mapper.LessonMapper;
import seryakov.practice33.model.Lesson;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class LessonDaoImpl extends AbstractDao<Lesson, Integer> {


    public LessonDaoImpl(ConnectionManagerJdbc connectionManagerJdbc) {
        super(connectionManagerJdbc);
    }

    @Override
    protected void initializeModelMapper() {
        modelMapper = new LessonMapper();
    }

    @Override
    protected String getInsertSql() {
        return "INSERT  INTO lesson " +
                "(class_room, topic, date, group_id) " +
                "VALUES (?, ?, ?, ?)";
    }

    @Override
    protected String getUpdateSql() {
        return "UPDATE lesson SET class_room = ?, topic = ?, date = ?, group_id = ? " +
                "WHERE  id = ?";
    }

    @Override
    protected void setInsertPreparedStatementParams(PreparedStatement preparedStatement, Lesson lesson) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, lesson);
    }

    @Override
    protected void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, Lesson lesson) throws SQLException {
        setSimilarPreparedStatementParams(preparedStatement, lesson);
        int i = preparedStatement.getParameterMetaData().getParameterCount();
        preparedStatement.setInt(++i, lesson.getId());

    }

    private void setSimilarPreparedStatementParams(PreparedStatement preparedStatement, Lesson lesson) throws SQLException {
        int i = 1;
        preparedStatement.setString(i++, lesson.getClassRoom());
        preparedStatement.setString(i++, lesson.getTopic());
        preparedStatement.setDate(i++, new Date(lesson.getDate().getTime()));
        preparedStatement.setInt(i, lesson.getGroupId());
    }

    @Override
    protected String getTableName() {
        return "lesson";
    }
}
