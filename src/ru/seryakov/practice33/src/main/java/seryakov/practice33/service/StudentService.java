package seryakov.practice33.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seryakov.practice33.da.StudentDaoImpl;
import seryakov.practice33.service.dto.StudentDto;
import seryakov.practice33.service.mapper.StudentDtoConverter;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentDaoImpl studentDao;

    public StudentService(StudentDaoImpl studentDao) {
        this.studentDao = studentDao;
    }

    public StudentDto getStudent(Integer id) {
        return StudentDtoConverter.convertStudent2Dto(studentDao.findById(id));
    }

    public StudentDto updateStudent(StudentDto studentDto) {
        return StudentDtoConverter.convertStudent2Dto(studentDao.update(StudentDtoConverter.convertDto2Student(studentDto)));
    }

    public StudentDto insertStudent(StudentDto studentDto) {
        return StudentDtoConverter.convertStudent2Dto(studentDao.insert(StudentDtoConverter.convertDto2Student(studentDto)));
    }

    public Boolean deleteStudent(Integer id) {
        return studentDao.delete(id);
    }

    public List<StudentDto> findAll() {
        return StudentDtoConverter.convertStudents2DtoList(studentDao.findAll());
    }

    public List<StudentDto> findByGroup(Integer groupId) {
        return StudentDtoConverter.convertStudents2DtoList(studentDao.findByField("GROUP_ID", groupId));
    }

}
