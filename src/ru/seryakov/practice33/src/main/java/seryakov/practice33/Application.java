package seryakov.practice33;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import seryakov.practice33.da.CityDaoImpl;
import seryakov.practice33.model.City;
import seryakov.practice33.service.GradeService;
import seryakov.practice33.service.GroupService;
import seryakov.practice33.service.LessonService;
import seryakov.practice33.service.StudentService;
import seryakov.practice33.service.dto.GradeDto;
import seryakov.practice33.service.dto.GroupDto;
import seryakov.practice33.service.dto.LessonDto;
import seryakov.practice33.service.dto.StudentDto;

import java.util.Date;
import java.util.logging.Logger;


/**
 * Итоговое задание: реализовать до конца сервис, который позволяет добавлять, удалять, изменять студентов, занятия,
 * группы. Ставить и удалять студентам оценки за занятия (добавить таблицу для оценок). Получать списки студентов
 * (общий и по группам), список групп, список оценок за занятие, расписание занятий для студента и общее.
 */
@Component
public class Application {

    Logger log = Logger.getLogger(this.getClass().getName());

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GradeService gradeService;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private CityDaoImpl cityDao;

    public void test() {

        City defaultCity = cityDao.insert(new City().setCityName("DefaultCity"));


        GroupDto groupDto = groupService.insertGroup(new GroupDto().setGroupName("MyGroup").setGroupGraduating("2020"));
        log.info(groupDto.toString());

        StudentDto studentDto = studentService.insertStudent(new StudentDto().setFirstName("Max").setLastName("Fami").setMiddleName("Der")
                .setBirthDate(new Date()).setGroupId(groupDto.getId()).setCityId(defaultCity.getId()));

        log.info(studentDto.toString());

        LessonDto lessonDto = lessonService.insertLesson(new LessonDto().setClassRoom("32323").setDate(new Date()).setGroupId(groupDto.getId())
                .setTopic("LessonTopic"));

        for (int i = 0; i < 5; i++) {
            gradeService.insertGrade(new GradeDto().setLessonId(lessonDto.getId()).setStudentId(studentDto.getId())
                    .setGrade(i));
        }

        log.info(lessonService.findByStudent(studentDto.getId()).toString());
        log.info(gradeService.findByStudent(studentDto.getId()).toString());


    }

}
