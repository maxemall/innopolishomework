package seryakov.practice33.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import seryakov.practice33.service.GroupService;

@RestController
public class GroupController {

    @Autowired
    GroupService groupService;

    @RequestMapping("/groups")
    public String getGroups(
            Model model) {
        model.addAttribute("list", groupService.findAll());
        return "groups";
    }


}
