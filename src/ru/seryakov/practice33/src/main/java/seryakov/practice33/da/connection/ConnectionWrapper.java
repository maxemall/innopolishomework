package seryakov.practice33.da.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionWrapper implements MyConnection {
    private Connection connection;

    public ConnectionWrapper(Connection connection) {
        this.connection = connection;
    }


    @Override
    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }

    @Override
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, Integer integer) throws SQLException {
        return connection.prepareStatement(sql, integer);
    }

    @Override
    public boolean isClosed() throws SQLException {
        return connection.isClosed();
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }


}
