package seryakov.practice33.da;

import org.springframework.stereotype.Component;
import seryakov.practice33.da.connection.ConnectionManagerJdbc;
import seryakov.practice33.da.mapper.GroupMapper;
import seryakov.practice33.model.Group;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class GroupDaoImpl extends AbstractDao<Group, Integer> {

    public GroupDaoImpl(ConnectionManagerJdbc connectionManagerJdbc) {
        super(connectionManagerJdbc);
    }

    @Override
    protected void initializeModelMapper() {
        modelMapper = new GroupMapper();
    }

    @Override
    protected String getInsertSql() {
        return "INSERT INTO public.group" +
                "(group_name, group_graduating) " +
                "VALUES (?, ?)";
    }

    @Override
    protected String getUpdateSql() {
        return "UPDATE group SET group_name = ?, group_graduating = ? WHERE  id = ?";
    }

    @Override
    protected void setInsertPreparedStatementParams(PreparedStatement preparedStatement, Group group) throws SQLException {
        int i = 1;
        preparedStatement.setString(i++, group.getGroupName());
        preparedStatement.setString(i, group.getGroupGraduating());
    }

    @Override
    protected void setUpdatePreparedStatementParams(PreparedStatement preparedStatement, Group group) throws SQLException {
        int i = 1;
        preparedStatement.setString(i++, group.getGroupName());
        preparedStatement.setString(i++, group.getGroupGraduating());
        preparedStatement.setInt(i, group.getId());
    }

    @Override
    protected String getTableName() {
        return "public.group";
    }
}
