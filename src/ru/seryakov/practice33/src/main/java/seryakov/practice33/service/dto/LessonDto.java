package seryakov.practice33.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class LessonDto {
    private Integer id;
    private String classRoom;
    private String topic;
    private Date date;
    private Integer groupId;
}
