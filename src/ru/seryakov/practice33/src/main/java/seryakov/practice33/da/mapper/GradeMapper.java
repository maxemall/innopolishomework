package seryakov.practice33.da.mapper;


import seryakov.practice33.model.Grade;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GradeMapper implements ModelMapper<Grade> {

    public Grade mapResultSetToModel(ResultSet resultSet) throws SQLException {
        Grade grade = new Grade();
            grade.setId(resultSet.getInt("id")).setLessonId(resultSet.getInt("lesson_id"))
                .setStudentId(resultSet.getInt("student_id"))
                .setGrade(resultSet.getInt("grade"));
        return grade;
    }

}
