package seryakov.practice33.service.mapper;

import seryakov.practice33.model.Grade;
import seryakov.practice33.service.dto.GradeDto;

import java.util.ArrayList;
import java.util.List;

public class GradeDtoConverter {

    private GradeDtoConverter() {
    }

    public static GradeDto convertGrade2Dto(Grade grade) {
        return new GradeDto().setId(grade.getId()).setLessonId(grade.getLessonId()).setStudentId(grade.getStudentId()).setGrade(grade.getGrade());
    }

    public static List<GradeDto> convertGrades2DtoList(List<Grade> grades) {
        List<GradeDto> gradeDtos = new ArrayList<>();
        for (Grade grade : grades) {
            gradeDtos.add(convertGrade2Dto(grade));
        }
        return gradeDtos;
    }

    public static Grade convertDto2Grade(GradeDto gradeDto) {
        return new Grade().setId(gradeDto.getId()).setStudentId(gradeDto.getStudentId()).setLessonId(gradeDto.getLessonId()).setGrade(gradeDto.getGrade());
    }



}
