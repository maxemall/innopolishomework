package seryakov.practice33.service.mapper;

import seryakov.practice33.model.Lesson;
import seryakov.practice33.service.dto.LessonDto;

import java.util.ArrayList;
import java.util.List;

public class LessonDtoConverter {

    private LessonDtoConverter() {
    }

    public static LessonDto convertLesson2Dto(Lesson lesson) {
        return new LessonDto().setId(lesson.getId()).setClassRoom(lesson.getClassRoom()).setTopic(lesson.getTopic())
                .setDate(lesson.getDate()).setGroupId(lesson.getGroupId());
    }

    public static List<LessonDto> convertLessons2DtoList(List<Lesson> lessons) {
        List<LessonDto> lessonDtos = new ArrayList<>();
        for (Lesson lesson : lessons) {
            lessonDtos.add(convertLesson2Dto(lesson));
        }
        return lessonDtos;
    }

    public static Lesson convertDto2Lesson(LessonDto lessonDto) {
        return new Lesson().setId(lessonDto.getId()).setClassRoom(lessonDto.getClassRoom()).setTopic(lessonDto.getTopic())
                .setDate(lessonDto.getDate()).setGroupId(lessonDto.getGroupId());
    }


}
