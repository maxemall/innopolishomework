package ru.seryakov.task41;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/*  Написать класс MathBox, реализующий следующий функционал:
      1.    Конструктор на вход получает массив Number. Элементы не могут повторяться. Элементы массива внутри объекта раскладываются в подходящую коллекцию (выбрать самостоятельно).
        2.    Существует метод summator, возвращающий сумму всех элементов коллекции
        3.    Существует метод splitter, выполняющий поочередное деление всех хранящихся в объекте элементов на делитель, являющийся аргументом метода. Хранящиеся в объекте данные полностью заменяются результатами деления.
        4.    Необходимо правильно переопределить методы toString, hashCode, equals, чтобы можно было использовать MathBox для вывода данных на экран и хранение объектов этого класса в коллекциях (например, hashMap). Выполнение контракта обязательно!
        5.    В будущем может возникнуть необходимость расширения функционала класса. Например, замена хранящихся элементов по значению.
        В процессе реализации необходимо использовать Исключения, Коллекции, Родовые типы там, где это уместно.*/


public class MathBox<E extends Number> {


    enum NumberClazz {Integer, Double, BigDecimal}
    NumberClazz numberClazz;
    private Set<E> numbersSet = new HashSet<>();


    public MathBox(E[] numbers) {
        numbersSet.addAll(Arrays.asList(numbers));
        numberClazz = getNumberClass();
    }


    //Мне кажется с заданием что-то не так или я неправильно его понял. Нам необходимо либо делать наследников
    // MathBox для каждого класса наследника Number, а сам MathBox делать интерфейсом (что противоречит заданию), либо делать что-то вроде того, что ниже,
    //что выглядит странным
    //Второй вариант в task42


    private NumberClazz getNumberClass() {
        NumberClazz numberClazz = null;
        if (numbersSet.size() > 0) {
            try {
                numberClazz = NumberClazz.valueOf(numbersSet.toArray()[0].getClass().getSimpleName());
            } catch (IllegalArgumentException e) {
                System.out.println("Неподдерживаемый тип данных");
                throw e;
            }
        }
        return numberClazz;
    }

    public E summator() {

        switch (numberClazz) {
            case Integer:
                return getIntegerSum();
            case Double:
                return getDoubleSum();
            case BigDecimal:
                return getBigDecimalSum();
            default:
                throw new NumberFormatException();
        }
    }

    private E getBigDecimalSum() {
        BigDecimal bigDecimalResult = new BigDecimal(0);
        for (E number : numbersSet
        ) {
            bigDecimalResult = bigDecimalResult.add((BigDecimal) number);
        }
        return (E) bigDecimalResult;
    }

    private E getDoubleSum() {
        Double doubleResult = 0.0;
        for (E number : numbersSet
        ) {
            doubleResult += (Double) number;
        }
        return (E) doubleResult;
    }

    private E getIntegerSum() {
        Integer integerResult = 0;
        for (E number : numbersSet
        ) {
            integerResult += (Integer) number;
        }
        return (E) integerResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox<?> mathBox = (MathBox<?>) o;
        return numberClazz == mathBox.numberClazz &&
                Objects.equals(numbersSet, mathBox.numbersSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberClazz, numbersSet);
    }

}
