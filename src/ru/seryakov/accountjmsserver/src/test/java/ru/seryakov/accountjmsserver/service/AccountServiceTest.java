package ru.seryakov.accountjmsserver.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.seryakov.accountjmsserver.dao.Account;
import ru.seryakov.accountjmsserver.dao.AccountRepository;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;


    @Test
    public void findAll() {
        Account account = Account.builder().fullName("Full Name M").active(true).balance(new BigDecimal(100)).build();
        accountRepository.addAccount(account);
        assertNotNull(accountService.findAll());
    }
}