package ru.seryakov.accountjmsserver.dao;

import io.ebean.EbeanServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Component
public class AccountRepository {

    @Autowired
    EbeanServer ebeanServer;

    public void addAccount(Account account) {
        ebeanServer.save(account);
    }

    public List<Account> findAll() {
        return ebeanServer.find(Account.class).findList();
    }

    public Account findById(Integer id) {
        return  ebeanServer.find(Account.class, id);
    }

    public Account addMoney(Integer id, BigDecimal value) {
        Account account = ebeanServer.find(Account.class, id);
        account.setBalance(account.getBalance().add(value));
        ebeanServer.update(account);
        return ebeanServer.find(Account.class, id);
    }

    public Account update(Account account) {
        ebeanServer.update(account);
        return ebeanServer.find(Account.class, account.getId());
    }

    @Transactional(rollbackFor = Exception.class)
    public void transfer(Account accountFrom, Account accountTo, BigDecimal value) {
            accountFrom.setBalance(accountFrom.getBalance().subtract(value));
            accountTo.setBalance(accountTo.getBalance().add(value));
            ebeanServer.update(accountFrom);
            ebeanServer.update(accountTo);
    }
}
