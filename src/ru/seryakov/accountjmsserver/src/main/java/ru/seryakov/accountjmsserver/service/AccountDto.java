package ru.seryakov.accountjmsserver.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonAutoDetect
public class AccountDto {
    private Integer id;
    private String fullName;
    private BigDecimal balance;
    private Boolean active;
}
