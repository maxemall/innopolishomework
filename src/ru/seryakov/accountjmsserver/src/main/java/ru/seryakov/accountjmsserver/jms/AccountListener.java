package ru.seryakov.accountjmsserver.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ru.seryakov.accountjmsserver.service.AccountDto;
import ru.seryakov.accountjmsserver.service.AccountService;
import ru.seryakov.accountjmsserver.service.TransferDto;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.IOException;
import java.util.List;

@Log4j2
@Component("accountListener")
public class AccountListener {
    private static final String ACCOUNT_REQUEST_QUEUE = "account.in";
    private static final String ACCOUNT_RESPONSE_QUEUE = "account.out";
    private static final String MONEY_REQUEST_QUEUE = "money.in";
    private static final String MONEY_RESPONSE_QUEUE = "money.out";
    private static final String TRANSFER_REQUEST_QUEUE = "transfer.in";
    private static final String TRANSFER_RESPONSE_QUEUE = "transfer.out";
    AccountService accountService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    public AccountListener(AccountService accountService) {
        this.accountService = accountService;
    }

    @JmsListener(destination = ACCOUNT_REQUEST_QUEUE)
    @SendTo({ACCOUNT_RESPONSE_QUEUE})
    public Message receiveAccountMessage(final Message message) throws JsonProcessingException, JMSException {
        TextMessage resultMessage = new ActiveMQTextMessage();
        try {
            if (message instanceof TextMessage) {
                if (((TextMessage) message).getText().equals("null")) {
                    List<AccountDto> accounts = accountService.findAll();
                    resultMessage.setText(objectMapper.writeValueAsString(accounts));
                } else {
                    String msg = ((TextMessage) message).getText();
                    AccountDto account = null;
                    try {
                        account = accountService.findById(objectMapper.readValue(msg, Integer.class));
                    } catch (IOException e) {
                        log.error("Deserialize id account error " + e.getMessage());
                    }
                    resultMessage.setText(objectMapper.writeValueAsString(account));
                }
            }
        } catch (JMSException e) {
            log.warn("Error on getting message from " + ACCOUNT_REQUEST_QUEUE, e);
        }
        ((ActiveMQTextMessage) resultMessage).setCorrelationId(message.getJMSCorrelationID());
        return resultMessage;
    }

    @JmsListener(destination = MONEY_REQUEST_QUEUE)
    @SendTo(MONEY_RESPONSE_QUEUE)
    public Message receiveMoneyMessage(final Message message) {
        TextMessage resultMessage = new ActiveMQTextMessage();
        if (message instanceof TextMessage) {
            try {
                String strMessage = ((TextMessage) message).getText();
                AccountDto accountDto = objectMapper.readValue(strMessage, AccountDto.class);
                accountService.update(accountDto);
                resultMessage.setText(objectMapper.writeValueAsString(accountService.findById(accountDto.getId())));
            } catch (JMSException | IOException e) {
                log.error("Add money request error  " + e.getMessage());
            }
        }
        return resultMessage;
    }

    @JmsListener(destination = TRANSFER_REQUEST_QUEUE)
    @SendTo(TRANSFER_RESPONSE_QUEUE)
    public Message receiveTransferMessage(final Message message) {
        TextMessage resultMessage = new ActiveMQTextMessage();
        if (message instanceof TextMessage) {
            try {
                String strMessage = ((TextMessage) message).getText();
                TransferDto transferDto = objectMapper.readValue(strMessage, TransferDto.class);
                if (accountService.transfer(transferDto)) {
                    resultMessage.setText("true");
                } else resultMessage.setText("false");
            } catch (JMSException | IOException e) {
                log.error("Transfer money request error  " + e.getMessage());
            }
        }
        return resultMessage;
    }

}
