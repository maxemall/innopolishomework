package ru.seryakov.accountjmsserver.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.seryakov.accountjmsserver.dao.Account;
import ru.seryakov.accountjmsserver.dao.AccountRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ModelMapper modelMapper;

    public List<AccountDto> findAll() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream().map(account -> modelMapper.map(account, AccountDto.class)).collect(Collectors.toList());
    }

    public AccountDto findById(Integer id) {
        return modelMapper.map(accountRepository.findById(id), AccountDto.class);
    }

    public void  addMoney(Integer id, BigDecimal value) {
        accountRepository.addMoney(id, value);
    }

    public void update(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);
        accountRepository.update(account);
    }

    public boolean transfer(TransferDto transferDto) {
        try {
            Account accountFrom = accountRepository.findById(transferDto.getAccountFrom());
            Account accountTo = accountRepository.findById(transferDto.getAccountTo());
            accountRepository.transfer(accountFrom, accountTo, transferDto.getValue());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
