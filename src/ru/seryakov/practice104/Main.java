package ru.seryakov.practice104;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        AtomicInteger timer = new AtomicInteger(0);
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Tick tick = new Tick(timer, lock, condition);
        tick.start();
        Sout sout = new Sout(timer, 5, lock, condition);
        sout.start();

        /*Thread.sleep(10000);
        do {
            sout.interrupt();
        } while (sout.isInterrupted());
*/
/*        try {
            sout.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}
