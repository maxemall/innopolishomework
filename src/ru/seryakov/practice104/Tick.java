package ru.seryakov.practice104;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Tick extends Thread {
    Lock lock;
    Condition condition;
    AtomicInteger timer;

    public Tick(AtomicInteger timer, Lock lock, Condition condition) {
        this.timer = timer;
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {

        while (!isInterrupted()) {

            try {
                sleep(1000);
                lock.lock();
                timer.incrementAndGet();
                System.out.println(timer.get());
                condition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }


        }
    }
}

