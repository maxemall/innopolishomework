package ru.seryakov.practice104;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Sout extends Thread {

    Lock lock;
    AtomicInteger timer;
    Integer interval;
    Condition condition;

    public Sout(AtomicInteger timer, Integer interval, Lock lock, Condition condition) {
        this.timer = timer;
        this.interval = interval;
        this.lock = lock;
        this.condition = condition;
    }


    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                lock.lock();
                condition.await();
                if (timer.get() % interval == 0)
                    System.out.println("Divided 5...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

        }
    }
}

