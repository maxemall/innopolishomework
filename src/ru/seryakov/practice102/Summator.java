package ru.seryakov.practice102;

import java.util.concurrent.atomic.AtomicInteger;

public class Summator extends Thread {
    public Summator(AtomicInteger intgr) {
        this.intgr = intgr;
    }

    private AtomicInteger intgr;


    @Override
    public void run() {
        do {
            intgr.incrementAndGet();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!isInterrupted());

        System.out.println(intgr);
    }
}
