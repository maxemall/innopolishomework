package ru.seryakov.practice21;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
/*        ReverseArray reverseArray = new ReverseArray(10);
        reverseArray.fill();
        reverseArray.display();
        reverseArray.reverse();
        reverseArray.display();*/


        ArrayList<Person> personArrayList = new ArrayList<>();

        for (int i = 0; i < 10 ; i++) {
            personArrayList.add(new Person(String.valueOf((50 -i)), String.valueOf(i)));
        }

        for (Person person : personArrayList) {
            System.out.println(person.firstName);
        }


        Collections.sort(personArrayList);

        for (Person person : personArrayList) {
            System.out.println(person.firstName);
        }


    }
}
