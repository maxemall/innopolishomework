package ru.seryakov.practice21;

public class ReverseArray {
    private  int[] array;
    private int size;

    public ReverseArray(int size) {
        this.size = size;
        this.array = new int[size];
    }

    public void fill() {
        for (int i = 0; i < size; i++) {
            array[i] = i;
        }
    }

    public void reverse() {
        int[] tempMassive = new int[size];
        for (int i = 0; i < size; i++) {
            tempMassive[size-1-i] = array[i];
        }
        array = tempMassive;
    }

    public void display() {
        for (int item:array) {
            System.out.print(item + " ");
        }
    }

}
