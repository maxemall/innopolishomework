package ru.seryakov.practice21;

public class Person implements Comparable<Person> {
    String firstName;
    String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public int compareTo(Person o) {
        return (this.firstName.compareTo(o.firstName) != 0? this.firstName.compareTo(o.firstName): this.lastName.compareTo(o.lastName));
    }
}
