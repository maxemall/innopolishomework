package ru.seryakov.task21;


import ru.seryakov.task21.drinks.Drink;

import java.util.Scanner;

public class VendingMachine {
    private  Scanner scanner = new Scanner(System.in);
    private double money = 0;
    private Drink[] drinks;

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    private void addMoney(double money) {
        this.money = money;
    }

    private Drink getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    private void giveMeADrink(int key) {
        if (this.money > 0) {
            Drink drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public void setMoney() {
        System.out.println("Введите сумму:");
        addMoney(scanner.nextDouble());
    }

    public void setDrink() {
        Drink[] drinks = getDrinks();
        System.out.println("Выберите напиток:");

        for (int i = 0; i < drinks.length; i++) {
            System.out.println(i + ":" + drinks[i].getTitle() + " - " + drinks[i].getPrice());
        }
        giveMeADrink(scanner.nextInt());
    }

    public void setDrinks(Drink[] drinks) {
        this.drinks = drinks;
    }

    private Drink[] getDrinks() {
        return this.drinks;
    }
}
