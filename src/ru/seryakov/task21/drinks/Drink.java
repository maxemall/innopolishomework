package ru.seryakov.task21.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}