package ru.seryakov.task21.drinks;

public enum Drinks implements Drink {
    TEA ("Чай", 150), COLA ("Кола", 45);

    private String title;
    private double price;

    Drinks(String title, double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getTitle() {
        return this.title;
    }
}
