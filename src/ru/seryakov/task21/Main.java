package ru.seryakov.task21;


import ru.seryakov.task21.drinks.Drink;
import ru.seryakov.task21.drinks.Drinks;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        VendingMachine vm = new VendingMachine(Drinks.values());

        vm.setMoney();
        vm.setDrink();

    }
}