package ru.seryakov.task71_Basket;

import java.util.*;

public class BasketImpl implements Basket {

    private Map<String, Integer> cart = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        if (cart.containsKey(product))
            cart.put(product, cart.get(product) + quantity);
        else cart.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        if (cart.containsKey(product))
            cart.remove(product);
        else throw new NoSuchElementException();
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (cart.containsKey(product))
            cart.put(product, quantity);
        else throw new NoSuchElementException();
    }

    @Override
    public void clear() {
        cart.clear();
    }

    @Override
    public List<String> getProducts() {
        if (!cart.isEmpty())
        return new ArrayList<>(cart.keySet());
        else throw new IllegalStateException();
    }

    @Override
    public int getProductQuantity(String product) {
        if (cart.containsKey(product))
            return cart.get(product);
        else throw new NoSuchElementException();

    }
}
