package ru.seryakov.practice27pattern.immutable;

public class MyComplexType {
    private Integer integer;
    private String string;

    public MyComplexType(Integer integer, String string) {
        this.integer = integer;
        this.string = string;
    }
}
