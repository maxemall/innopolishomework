package ru.seryakov.practice27pattern.abstractFactory;

public abstract class Vehicle {
    private Wheel wheel;
    private Integer wheelNumber;
    private Integer clearance;
    private Rigidity rigidity;
    private String style;

    public Vehicle(Wheel wheel, Integer wheelNumber, Integer clearance, Rigidity rigidity, String style) {
        this.wheel = wheel;
        this.wheelNumber = wheelNumber;
        this.clearance = clearance;
        this.rigidity = rigidity;
        this.style = style;
    }

    public String getStyle() {
        return this.getClass().getSimpleName() + " " + style;
    }
}
