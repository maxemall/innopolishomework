package ru.seryakov.practice27pattern.abstractFactory;

public class Velobike extends Vehicle {


    public Velobike(Wheel wheel, Integer wheelNumber, Integer clearance, Rigidity rigidity, String style) {
        super(wheel, wheelNumber, clearance, rigidity, style);
    }
}
