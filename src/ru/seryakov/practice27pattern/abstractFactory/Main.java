package ru.seryakov.practice27pattern.abstractFactory;

public class Main {

    private VehicleFactory vehicleFactory;
    private Vehicle vehicle;

    public Main(VehicleFactory vehicleFactory) {
        this.vehicleFactory = vehicleFactory;
    }

    public void createCar() {
        vehicle = vehicleFactory.createCar();
    }

    public void showStyle() {
        System.out.println(vehicle.getStyle());
    }

    public static void main(String[] args) {
        Main main = new Main(new DailyFactory());
        main.createCar();
        main.showStyle();

        Main main2 = new Main(new OffroadFactory());
        main2.createCar();
        main2.showStyle();
    }
}
