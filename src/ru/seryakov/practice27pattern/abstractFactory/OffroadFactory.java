package ru.seryakov.practice27pattern.abstractFactory;

public class OffroadFactory implements VehicleFactory {
    @Override
    public Car createCar() {
        return new Car(Wheel.OFFROAD, 4, 220, Rigidity.OFFROAD, "offroad");
    }

    @Override
    public Motobike createMotobike() {
        return new Motobike(Wheel.OFFROAD, 2, 60, Rigidity.OFFROAD, "offroad");
    }

    @Override
    public Velobike createVelobike() {
        return new Velobike(Wheel.OFFROAD, 2, 60, Rigidity.OFFROAD, "offroad");
    }
}
