package ru.seryakov.practice27pattern.abstractFactory;

public interface VehicleFactory {
    public Car createCar();
    public Motobike createMotobike();
    public Velobike createVelobike();
}
