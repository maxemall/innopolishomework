package ru.seryakov.practice27pattern.abstractFactory;

public enum Wheel {
    ROAD, OFFROAD, DAILY;

    Wheel() {
    }
}
