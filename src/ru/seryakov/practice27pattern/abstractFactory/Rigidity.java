package ru.seryakov.practice27pattern.abstractFactory;

public enum Rigidity {
    ROAD, OFFROAD, DAILY;
}
