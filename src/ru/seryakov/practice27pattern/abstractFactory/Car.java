package ru.seryakov.practice27pattern.abstractFactory;

public class Car extends Vehicle {

    public Car(Wheel wheel, Integer wheelNumber, Integer clearance, Rigidity rigidity, String style) {
        super(wheel, wheelNumber, clearance, rigidity, style);
    }
}

