package ru.seryakov.practice27pattern.abstractFactory;

public class Motobike extends Vehicle {

    public Motobike(Wheel wheel, Integer wheelNumber, Integer clearance, Rigidity rigidity, String style) {
        super(wheel, wheelNumber, clearance, rigidity, style);
    }
}
