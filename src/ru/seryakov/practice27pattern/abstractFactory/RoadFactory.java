package ru.seryakov.practice27pattern.abstractFactory;

public class RoadFactory implements VehicleFactory {
    @Override
    public Car createCar() {
        return new Car(Wheel.ROAD, 4, 100, Rigidity.ROAD, "road");
    }

    @Override
    public Motobike createMotobike() {
        return new Motobike(Wheel.ROAD, 2, 20, Rigidity.ROAD, "road");
    }

    @Override
    public Velobike createVelobike() {
        return new Velobike(Wheel.ROAD, 2, 40, Rigidity.ROAD, "road");
    }
}
