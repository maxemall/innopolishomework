package ru.seryakov.practice27pattern.abstractFactory;

public class DailyFactory implements VehicleFactory {
    @Override
    public Car createCar() {
        return new Car(Wheel.DAILY, 4, 160, Rigidity.DAILY, "daily");
    }

    @Override
    public Motobike createMotobike() {
        return new Motobike(Wheel.DAILY, 2, 30, Rigidity.DAILY, "daily");
    }

    @Override
    public Velobike createVelobike() {
        return new Velobike(Wheel.DAILY, 2, 50, Rigidity.DAILY, "daily");
    }
}
