package ru.seryakov.practice27pattern.builder;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        CarDirector carDirector = new CarDirector();
        Car car = carDirector.makeLuxe();
        Car car2 = (Car) car.clone();
        System.out.println(car2.equals(car));
        System.out.println((car == car2));
    }
}
