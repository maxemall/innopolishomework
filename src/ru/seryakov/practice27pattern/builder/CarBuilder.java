package ru.seryakov.practice27pattern.builder;

public interface CarBuilder {
    public CarBuilder Reset();
    public CarBuilder buildClimat(Climat climat);
    public CarBuilder buildEngine(Engine engine);
    public CarBuilder buildHeadlight(Headlight headlight);
    public CarBuilder buildMedia(Media media);
    public CarBuilder buildSalon(Salon salon);
    public CarBuilder buildWheel(Wheel wheel);
    public Car build();

}
