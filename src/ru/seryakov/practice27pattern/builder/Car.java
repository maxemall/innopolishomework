package ru.seryakov.practice27pattern.builder;

import java.util.Objects;

public class Car implements Cloneable {
    private Climat climat;
    private Engine engine;
    private Headlight headlight;
    private Media media;
    private Salon salon;
    private Wheel wheel;

    public Climat getClimat() {
        return climat;
    }

    public void setClimat(Climat climat) {
        this.climat = climat;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Headlight getHeadlight() {
        return headlight;
    }

    public void setHeadlight(Headlight headlight) {
        this.headlight = headlight;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Salon getSalon() {
        return salon;
    }

    public void setSalon(Salon salon) {
        this.salon = salon;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Car car = new Car();
        car.climat = this.climat;
        car.engine = this.engine;
        car.headlight = this.headlight;
        car.media = this.media;
        car.salon = this.salon;
        car.wheel = this.wheel;
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return climat == car.climat &&
                engine == car.engine &&
                headlight == car.headlight &&
                media == car.media &&
                salon == car.salon &&
                wheel == car.wheel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(climat, engine, headlight, media, salon, wheel);
    }
}
