package ru.seryakov.practice27pattern.builder;

public class CarBuilderImpl implements CarBuilder {
    private Car car;

    @Override
    public CarBuilder Reset() {
        car = new Car();
        return this;
    }

    @Override
    public CarBuilder buildClimat(Climat climat) {
        this.car.setClimat(climat);
        return this;
    }

    @Override
    public CarBuilder buildEngine(Engine engine) {
        this.car.setEngine(engine);
        return this;
    }

    @Override
    public CarBuilder buildHeadlight(Headlight headlight) {
        this.car.setHeadlight(headlight);
        return this;
    }

    @Override
    public CarBuilder buildMedia(Media media) {
        this.car.setMedia(media);
        return this;
    }

    @Override
    public CarBuilder buildSalon(Salon salon) {
        this.car.setSalon(salon);
        return this;
    }

    @Override
    public CarBuilder buildWheel(Wheel wheel) {
        this.car.setWheel(wheel);
        return this;
    }

    @Override
    public Car build() {
        return this.car;
    }
}
