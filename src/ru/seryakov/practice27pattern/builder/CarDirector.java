package ru.seryakov.practice27pattern.builder;

public class CarDirector {
    private CarBuilder carBuilder;

    public CarDirector() {
        carBuilder = new CarBuilderImpl();
    }

    public Car makeLuxe() {
        return carBuilder.Reset().buildClimat(Climat.CLIMAT).buildEngine(Engine.F24).buildHeadlight(Headlight.XENON).
                buildMedia(Media.SUPER).buildSalon(Salon.LEATHER).buildWheel(Wheel.R18).build();
    }

    public Car makeStandart() {
        return carBuilder.Reset().buildClimat(Climat.CONDITION).buildEngine(Engine.F20).buildHeadlight(Headlight.DIOD).
                buildMedia(Media.FLASH).buildSalon(Salon.VELOURS).buildWheel(Wheel.R17).build();
    }

    public Car makeSimple() {
        return carBuilder.Reset().buildClimat(Climat.NONE).buildEngine(Engine.T15).buildHeadlight(Headlight.HALOGEN).
                buildMedia(Media.CASSET).buildSalon(Salon.DERMANTIN).buildWheel(Wheel.R16).build();
    }
}
