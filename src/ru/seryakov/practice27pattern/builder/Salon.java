package ru.seryakov.practice27pattern.builder;

public enum Salon {
    DERMANTIN, VELOURS, LEATHER;
}
