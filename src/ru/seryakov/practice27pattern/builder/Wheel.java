package ru.seryakov.practice27pattern.builder;

public enum Wheel {
    R16, R17, R18;
}
