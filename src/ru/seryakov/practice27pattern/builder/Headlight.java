package ru.seryakov.practice27pattern.builder;

public enum Headlight {
    XENON, HALOGEN, DIOD;
}
