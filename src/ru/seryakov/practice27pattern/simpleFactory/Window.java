package ru.seryakov.practice27pattern.simpleFactory;

import java.util.List;

public abstract class Window {
    private Integer x;
    private Integer y;
    private Integer height;
    private Integer width;
    private List<Button> buttonList;

    public Window(Integer x, Integer y, Integer height, Integer width, List<Button> buttonList) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.buttonList = buttonList;
    }
}
