package ru.seryakov.practice27pattern.simpleFactory;

public abstract class MyThread {
    public abstract Window createWindow();
}
