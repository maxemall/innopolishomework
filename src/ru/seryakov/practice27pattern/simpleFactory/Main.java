package ru.seryakov.practice27pattern.simpleFactory;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Window window2 = WindowsFactory.createWindow("ru.seryakov.practice27pattern.simpleFactory.StandartWindow");
        Window window = WindowsFactory.createWindow("ru.seryakov.practice27pattern.simpleFactory.DialogWindow");

        if (window instanceof DialogWindow) {
            System.out.println("ok");
        } else {
            System.out.println("no");
        }
        if (window2 instanceof StandartWindow) {
            System.out.println("ok");
        } else {
            System.out.println("no");
        }
    }
}
