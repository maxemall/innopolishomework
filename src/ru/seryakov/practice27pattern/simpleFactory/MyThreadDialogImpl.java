package ru.seryakov.practice27pattern.simpleFactory;

import java.util.ArrayList;

public class MyThreadDialogImpl extends MyThread {
    @Override
    public Window createWindow() {
        return new DialogWindow(1,1,1,1, new ArrayList<>());
    }
}
