package ru.seryakov.practice27pattern.simpleFactory;

import java.util.List;

public class StandartWindow extends Window {
    public StandartWindow(Integer x, Integer y, Integer height, Integer width, List<Button> buttonList) {
        super(x, y, height, width, buttonList);
        buttonList.add(new Button(1, 1, "Развернуть"));
        buttonList.add(new Button(2, 2, "Свернуть"));
        buttonList.add(new Button(3, 3, "Закрыть"));

    }
}
