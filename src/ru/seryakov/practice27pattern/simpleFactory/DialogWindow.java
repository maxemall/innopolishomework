package ru.seryakov.practice27pattern.simpleFactory;

import java.util.List;

public class DialogWindow extends Window {
    public DialogWindow(Integer x, Integer y, Integer height, Integer width, List<Button> buttonList) {
        super(x, y, height, width, buttonList);
        buttonList.add(new Button(1, 1, "ok"));
    }
}
