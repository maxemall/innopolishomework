package ru.seryakov.practice27pattern.simpleFactory;


public class Button {
    int x;
    int y;
    String text;

    public Button(int x, int y, String text) {
        this.x = x;
        this.y = y;
        this.text = text;
    }
}
