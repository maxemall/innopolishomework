package ru.seryakov.practice27pattern.simpleFactory;

import java.util.ArrayList;

public class MyThreadStandartImpl extends MyThread {
    @Override
    public Window createWindow() {
        return new StandartWindow(1,1,1,1, new ArrayList<>());
    }
}
