package ru.seryakov.practice27pattern.simpleFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class WindowsFactory {
    public static Window createWindow(String windowType) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Class.forName(windowType);
        Constructor<Window> constructor = clazz.getConstructor(Integer.class, Integer.class, Integer.class, Integer.class, List.class);
        return constructor.newInstance(1, 1, 1, 1, new ArrayList<>());
    }
}
