package ru.seryakov.task91;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MathBoxHandler implements InvocationHandler {
    private MathBox mathBox;

    public MathBoxHandler(MathBox mathBox) {
        this.mathBox = mathBox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        boolean logData = annotatedMethodPresent(LogData.class, method);
        boolean clearData = annotatedMethodPresent(ClearData.class, method);
        if (logData) {
            printNumberSet();
            System.out.println(method.getName());
        }
        result = method.invoke(mathBox, args);
        if (logData) printNumberSet();
        if (clearData) mathBox.clearData();
        return result;
    }

    private boolean annotatedMethodPresent(Class clazz, Method method) throws NoSuchMethodException {
        return mathBox.getClass().getMethod(method.getName(), method.getParameterTypes()).getAnnotation(clazz) != null;
    }

    private void printNumberSet() {
        System.out.println("Коллекция ----------------->");
        for (Integer number : mathBox.getNumbersSet()) {
            System.out.println(number);
        }
        System.out.println("<-------------------------->");
    }
}
