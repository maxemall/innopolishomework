package ru.seryakov.task91;

import java.util.*;

public class MathBoxImpl implements MathBox {
    private Set<Integer> numbersSet = new HashSet<>();

    public MathBoxImpl(Integer[] numbers) {
        numbersSet.addAll(Arrays.asList(numbers));
    }

    public MathBoxImpl() {
    }

    @Override
    @LogData
    public boolean addElement(Integer element) {
        return numbersSet.add(element);
    }

    @Override
    public boolean removeElement(Integer element) {
        return numbersSet.remove(element);
    }


    /**
     * @return 0 when numberSet is empty
     */
    @Override
    public Integer getTotal() {
        Integer result = 0;
        for (Integer number : numbersSet) {
            result = result + number;
        }
        return result;
    }

    /**
     * @return NaN when numberSet is empty
     */
    @Override
    @ClearData
    @LogData
    public Double getAverage() {
        return Double.valueOf(getTotal())/numbersSet.size();
    }

    @Override
    public Set<Integer> getMultiplied(int multiplicator) {
        Set<Integer> bufferSet = new HashSet<>();

        for (Integer number:numbersSet) {
            bufferSet.add(number * multiplicator);
        }

        return bufferSet;
    }

    @Override
    public Map<Integer, String> getMap() {
        Map<Integer, String> resultMap = new HashMap<>();
        for (Integer number : numbersSet) {
            resultMap.put(number, String.valueOf(number));
        }
        return resultMap;
    }

    @Override
    public Integer getMax() {
        return Collections.max(numbersSet);
    }

    @Override
    public Integer getMin() {
        return Collections.min(numbersSet);
    }

    public void clearData() {
        numbersSet.clear();
    }

    public Set<Integer> getNumbersSet() {
        return numbersSet;
    }
}
