package ru.seryakov.task91;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        final Integer[] elements = {5, 6, 7, 8};
        final Integer[] elementsInit = {1, 2, 3, 4};

        MathBox mathBox = new MathBoxImpl(elementsInit);

        MathBox mathBoxHandler = (MathBox) Proxy.newProxyInstance(MathBox.class.getClassLoader(), new Class[] {MathBox.class}, new MathBoxHandler(mathBox)) ;

        mathBox.addElement(elements[0]);
        mathBoxHandler.addElement(elements[0]);
        mathBoxHandler.addElement(elements[1]);

        System.out.println(mathBox.getAverage());
        System.out.println(mathBoxHandler.getAverage());

        mathBoxHandler.addElement(elements[0]);
    }
}
