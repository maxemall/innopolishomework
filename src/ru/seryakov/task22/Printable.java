package ru.seryakov.task22;

public interface Printable {
    boolean print();
}
