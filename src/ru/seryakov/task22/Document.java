package ru.seryakov.task22;

import java.util.Date;

public abstract class Document implements Printable {
    private String title;
    private Date docDate;

    public Document(String title) {
        this.title = title;
        this.docDate = new Date();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }
}
