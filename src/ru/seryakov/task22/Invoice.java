package ru.seryakov.task22;

public class Invoice extends Document {

    int ttnNumber;

    public Invoice(String title, int ttnNumber) {
        super(title);
        this.ttnNumber = ttnNumber;
    }

    @Override
    public boolean print() {
        System.out.println("Это документ заказ " + this.getTitle());
        return true;
    }

    public int getTtnNumber() {
        return ttnNumber;
    }

    public void setTtnNumber(int ttnNumber) {
        this.ttnNumber = ttnNumber;
    }
}
