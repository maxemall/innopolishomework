package ru.seryakov.task22;

import java.util.List;

public class Order extends Document {
    private List<String> orderPositions;

    public Order(String title) {
        super(title);
    }

    public List<String> getOrderPositions() {
        return orderPositions;
    }

    public void setOrderPositions(List<String> orderPositions) {
        this.orderPositions = orderPositions;
    }

    @Override
    public boolean print() {
        System.out.println("Это документ заказ " + this.getTitle());
        return true;
    }
}
