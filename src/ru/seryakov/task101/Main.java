package ru.seryakov.task101;

import ru.seryakov.task14.Factorial;

/**
 * Расчет факториала в несколько потоков двумя алгоритмами
 * Сначала сделал задание как озвучивали устно, потом в moodle условия поменялись
 * Переделывать не стал, так как по сути алгоритм расчета по диапазонам реализован, а эта задача была интереснее.
 * Так например, управление потоками через ArrayList самостоятельно, работает значительно быстрее, нежели ThreadPool
 * Для расчета коллекции, как в задании, ее просто надо предварительно отсортировать, а потом считать по интервалам
 */
public class Main {

    private static final int LIMIT = 10_000;
    private static final int THREADS_NUMBER = 11;

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        System.out.println("Рассчет в один поток результат: " + Factorial.calcFactorial(LIMIT));
        System.out.println("Рассчет в один поток время: " + (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        System.out.println("Рассчет 1 способ в " + THREADS_NUMBER + " потоков результат: " + new CalcRunner1(THREADS_NUMBER, LIMIT).calc());
        System.out.println("Рассчет 1 способ в " + THREADS_NUMBER + " потоков время: " + (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        System.out.println("Рассчет 2 способ в " + THREADS_NUMBER + " потоков результат: " + new CalcRunner2(THREADS_NUMBER, LIMIT).calc());
        System.out.println("Рассчет 2 способ в " + THREADS_NUMBER + " потоков время: " + (System.currentTimeMillis() - startTime));

    }
}
