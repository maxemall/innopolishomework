package ru.seryakov.task101;

import java.util.concurrent.*;


/**
 * Расчитывает факториал перемножением чисел с шагом равным количеству потоков. Например, для 3-х потоков
 * перемножаются 1, 4, 7, 10 в первом, 2, 5, 8, 11 во втором и т.д.
 */
public class CalcRunner2 extends CalcRunner1 {


    public CalcRunner2(int threadsNumber, int limit) {
        super(threadsNumber, limit);
    }

    @Override
    protected void runThreads(CountDownLatch finished) {

        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);

        for (int i = 1; i <= threadsNumber; i++) {
            resultList.add(executorService.submit(new CalcFThread(limit, i, threadsNumber, finished)));
        }
        executorService.shutdown();
    }
}