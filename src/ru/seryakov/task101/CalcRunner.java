package ru.seryakov.task101;

import java.math.BigInteger;

public interface CalcRunner {
    BigInteger calc();

    BigInteger multiplyThreadResults();
}
