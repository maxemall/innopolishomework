package ru.seryakov.task101;

import ru.seryakov.task14.Factorial;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class CalcFThread implements Callable<BigInteger> {

    private BigInteger factorial;

    private CountDownLatch finished;
    private int from;
    private int limit;
    private int step;

    public CalcFThread(int limit, int from, int step, CountDownLatch finished) {
        this.from = from;
        this.limit = limit;
        this.step = step;
        this.finished = finished;
    }

    @Override
    public BigInteger call() {
        factorial = Factorial.calcFactorial(limit, from, step);
        finished.countDown();
        return factorial;
    }

}
