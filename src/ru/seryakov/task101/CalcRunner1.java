package ru.seryakov.task101;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


/**
 * Расчитывает факториал путем деления на равные последовательные интервалы
 */
public class CalcRunner1 implements CalcRunner {


    protected int threadsNumber;
    protected int limit;
    protected List<Future<BigInteger>> resultList = new ArrayList<>();


    public CalcRunner1(int threadsNumber, int limit) {
        this.threadsNumber = threadsNumber;
        this.limit = limit;
    }

    @Override
    public BigInteger calc() {
        CountDownLatch finished = new CountDownLatch(threadsNumber);
        runThreads(finished);

        try {
            finished.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return multiplyThreadResults();
    }

    @Override
    public BigInteger multiplyThreadResults() {
        BigInteger result = new BigInteger("1");

        for (Future<BigInteger> fThreadResult : resultList) {
            try {
                result = result.multiply(fThreadResult.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    protected void runThreads(CountDownLatch finished) {
        int modulo = limit % threadsNumber;
        int divInteger = limit / threadsNumber;
        int limit = divInteger + modulo;
        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);

        for (int i = 0; i < threadsNumber; i++) {
            if (i == 0) resultList.add(executorService.submit(new CalcFThread(limit, 1, 1, finished)));
            else
                resultList.add(executorService.submit(new CalcFThread(limit + divInteger * i, limit + divInteger * (i - 1) + 1, 1, finished)));
        }
        executorService.shutdown();
    }
}