package ru.seryakov.task72_uniqueChars;

import java.util.Hashtable;
import java.util.Map;

public class UniqueChars {

    private String text;
    Map<Character, Integer> hashtable;

    public String getText() {
        calculate();
        return mapToString();
    }

    private String mapToString() {
        String result = "";
        for (Character c: hashtable.keySet()) {
            result = result + "\"" + c + "\" =>" + hashtable.get(c).toString() + "\n";
        }
        return result;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        hashtable = new Hashtable<>();
        for (char c: text.toCharArray()) {
            if (hashtable.containsKey(c))
                hashtable.put(c, hashtable.get(c) + 1);
                else hashtable.put(c, 1);
        }
    }


}

