package ru.seryakov.task14;

import ru.seryakov.task13.MultiplicationTable;

import java.math.BigInteger;


//4) Написать программу, которая выводит факториал для N чисел.
public class Factorial {
    public static void main(String[] args) {
        int limit = MultiplicationTable.getLimit();
        BigInteger result = calcFactorial(limit);
        System.out.println(result.bitLength());
    }

    public static BigInteger calcFactorial(int limit) {
        return calcFactorial(limit, 1, 1);
    }

    public static BigInteger calcFactorial(int limit, int from, int step) {
        BigInteger result = BigInteger.ONE;

        if (limit <= 0) throw  new IllegalArgumentException("Value cannot be less then 0");
        for (int i = from; i <= limit; i = i + step) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}


