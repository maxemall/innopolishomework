package ru.seryakov.task13;

import java.util.Scanner;

//3) Написать программу для вывода на экран таблицы умножения до введенного пользователем порога.
public class MultiplicationTable {
    public static void main(String[] args) {


        int limit = getLimit();

        int[][] table = getTable(limit);

        printTable(table);
    }

    private static int getCountsOfDigits(int number) {
        return String.valueOf(Math.abs(number)).length();
    }

    private static void printTable(int[][] table) {
        int countOfDigits = getCountsOfDigits(table.length * table[0].length) + 1;
        for (int[] aTable : table) {
            System.out.println();
            for (int j = 0; j < table[0].length; j++) {
                System.out.printf("%-" + countOfDigits + "s", aTable[j]);
            }
        }
    }

    private static int[][] getTable(int limit) {
        int[][] table = new int[limit][limit];
        for (int i = 0; i <= limit - 1; i++) {
            for (int j = 0; j <= limit - 1; j++) {
                table[i][j] = (i + 1) * (j + 1);
            }
        }
        return table;
    }

    public static int getLimit() {
        Scanner scanner = new Scanner(System.in);
        int limit = 0;
        System.out.println("Input limit");

        try {
            limit = scanner.nextInt();
        } catch (NumberFormatException e) {
            System.out.println("Arguments illegal type.");
        }
        return limit;
    }
}


