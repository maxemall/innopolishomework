package ru.seryakov.practice103;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger timer = new AtomicInteger(0);
        Tick tick = new Tick(timer);
        tick.start();
        Sout sout = new Sout(timer, 5);
        sout.start();

        Thread.sleep(10000);
        do {
            sout.interrupt();
        } while (sout.isInterrupted());

/*        try {
            sout.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}
