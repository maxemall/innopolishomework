package ru.seryakov.practice103;

import java.util.concurrent.atomic.AtomicInteger;

public class Tick extends Thread {

    AtomicInteger timer;

    public Tick(AtomicInteger timer) {
        this.timer = timer;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (timer) {

                timer.incrementAndGet();
                timer.notifyAll();
                System.out.println(timer.get());
            }
        }
    }
}
