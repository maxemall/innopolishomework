package ru.seryakov.practice103;

import java.util.concurrent.atomic.AtomicInteger;

public class Sout extends Thread {

    AtomicInteger timer;
    Integer interval;

    public Sout(AtomicInteger timer, Integer interval) {
        this.timer = timer;
        this.interval = interval;
    }


    @Override
    public void run() {
        while (!isInterrupted()) {
            synchronized (timer) {
                try {
                    timer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (timer.get() % interval == 0)
                    System.out.println("Waited...");
            }
        }
    }
}
