package ru.seryakov.practice103;

        /*Задача Хронометр
        Напишите программу, которая каждую секунду отображает на экране данные о времени, прошедшем от начала сессии,
        а другой ее поток выводит сообщение каждые 5 секунд. Предусмотрите возможность ежесекундного оповещения потока,
        воспроизводящего сообщение, потоком, отсчитывающим время. Не внося изменений в код потока-"хронометра" ,
        добавьте еще один поток, который выводит на экран другое сообщение каждые 7 секунд. Предполагается использование
        методов wait(), notifyAll().*/

import java.util.concurrent.atomic.AtomicInteger;

public class Chrono extends Thread {

    AtomicInteger timer = new AtomicInteger(0);
    String message;
    AtomicInteger interval;


    public Chrono(String message, AtomicInteger interval) {
        super();
        this.message = message;
        this.interval = interval;
    }

    @Override
    public void run() {
        do {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timer.incrementAndGet();

            if (interval.get() == 0) {
                System.out.println(timer.get());
            }
            else if (timer.get() % interval.get() == 0) System.out.println(message);
        } while (!isInterrupted());
        System.out.println("isInterrupted");
    }
}
