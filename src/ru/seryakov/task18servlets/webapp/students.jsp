<%--
  Created by IntelliJ IDEA.
  User: maksim.seryakov
  Date: 2019-02-09
  Time: 12:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Students</title>
</head>
<body>
<c:forEach var="student" items="${list}">
    <p><table>
    <tr> <td>${student.name}</td><td><a href = 'removeStudent?studentId=${student["id"]}&groupId=${groupId}'>Удалить</a></td> </tr>
</table></p>
</c:forEach>

<br>
<form title="Добавить студента в группу" action="/addStudent" method="post">
    <input type="text" name="studentName">
    <input type="text" name="city">
    <input type="datetime-local" name="birthDate">
    <input type="hidden" name="groupId" value="${groupId}">
    <input type="submit">
</form>
<br>
<a href="groups">back</a>
</body>
</html>
