package DTO;

import java.util.Calendar;

public class Student {
    private int id;
    private String Name;
    private String City;
    private Calendar birthDate;

    public Student(int id, String name, String city, Calendar birthDate) {
        this.id = id;
        Name = name;
        City = city;
        this.birthDate = birthDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }
}
