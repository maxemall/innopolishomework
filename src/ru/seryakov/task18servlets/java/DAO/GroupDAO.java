package DAO;

import DTO.Group;
import DTO.Student;

import java.util.List;

public interface GroupDAO {
    Group findGroup(int id);
    List<Group> findAllGroups();
    Group createGroup(String groupName);
    void deleteGroup(int id);
    Group updateGroup(int id, String groupName);
    List<Student> findGroupStudents(int id);
    void addStudent2Group(int id, Student student);
    void removeStudentFromGroup(int id, Student student);
}
