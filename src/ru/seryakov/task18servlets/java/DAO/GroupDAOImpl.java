package DAO;

import DTO.Group;
import DTO.Student;

import java.util.*;

public class GroupDAOImpl implements GroupDAO {
    private Map<Integer, Group> groupMap = new HashMap<>();

    private GroupDAOImpl() {
        groupMap.put(1, new Group(1, "Group1"));
        groupMap.put(2, new Group(2, "Group2"));
        groupMap.put(3, new Group(3, "Group3"));
    }

    public GroupDAOImpl(StudentDAO studentDAO) {
        this();
        addStudent2Group(1, studentDAO.findStudent(1));
        addStudent2Group(1, studentDAO.findStudent(2));
        addStudent2Group(2, studentDAO.findStudent(3));
        addStudent2Group(2, studentDAO.findStudent(4));
        addStudent2Group(3, studentDAO.findStudent(5));
    }

    public Group findGroup(int id) {
        return groupMap.get(id);
    }

    public List<Group> findAllGroups() {
        return new ArrayList<>(groupMap.values());
    }

    public Group createGroup(String name) {
        int id = groupMap.size() + 1;
        return groupMap.put(id, new Group(id, name));
    }

    public void deleteGroup(int id) {

    }

    public Group updateGroup(int id, String name) {
        return groupMap.put(id, new Group(id, name));
    }

    @Override
    public List<Student> findGroupStudents(int id) {
        return groupMap.get(id).getStudents();
    }

    @Override
    public void addStudent2Group(int id, Student student) {
        groupMap.get(id).addStudent(student);
    }

    @Override
    public void removeStudentFromGroup(int id, Student student) {
        groupMap.get(id).removeStudent(student);
    }
}
