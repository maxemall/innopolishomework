package DAO;

import DTO.User;

import java.util.List;

public interface UserDAO {
    User findUser(int id);
    List<User> findAllUsers();
    User createUser(String userName, String password);
    void deleteUser(int id);
    User updateUser(int id, String userName, String password);
}
