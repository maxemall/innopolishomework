package DAO;

public class DaoFactory {
    private static volatile GroupDAO groupDAO;
    private static volatile UserDAO userDAO;
    private static volatile StudentDAO studentDAO;

    public static GroupDAO getGroupDAO() {
        GroupDAO localGroupDAO = groupDAO;

        if (localGroupDAO == null) {
            synchronized (GroupDAO.class) {
                localGroupDAO = groupDAO;
                if (localGroupDAO == null) {
                    groupDAO = localGroupDAO = new GroupDAOImpl(getStudentDAO());
                }
            }
        }
        return groupDAO;
    }

    public static UserDAO getUserDAO() {
        UserDAO localUserDAO = userDAO;

        if (localUserDAO == null) {
            synchronized (UserDAO.class) {
                localUserDAO = userDAO;
                if (localUserDAO == null) {
                    userDAO = localUserDAO = new UserDAOImpl();
                }
            }
        }
        return userDAO;
    }

    public static StudentDAO getStudentDAO() {
        StudentDAO localStudentDAO = studentDAO;

        if (localStudentDAO == null) {
            synchronized (StudentDAO.class) {
                localStudentDAO = studentDAO;
                if (localStudentDAO == null) {
                    studentDAO = localStudentDAO = new StudentDAOImpl();
                }
            }
        }
        return studentDAO;
    }
}
