package DAO;

import DTO.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDAOImpl implements UserDAO {
    private Map<Integer, User> userMap = new HashMap<>();

    public UserDAOImpl() {
        userMap.put(1, new User(1, "1", "1"));
        userMap.put(2, new User(2, "user2", "password2"));
        userMap.put(3, new User(3, "user3", "password3"));

    }

    public User findUser(int id) {
        return userMap.get(id);
    }

    public List<User> findAllUsers() {
        return new ArrayList<>(userMap.values());
    }

    public User createUser(String userName, String password) {
        int id = userMap.size() + 1;
        return userMap.put(id, new User(id, userName, password));
    }

    public void deleteUser(int id) {
        userMap.remove(id);
    }

    public User updateUser(int id, String userName, String password) {
        return userMap.put(id, new User(id, userName, password));
    }
}
