package DAO;

import DTO.Student;

import java.util.*;

public class StudentDAOImpl implements StudentDAO {
    private Map<Integer, Student> studentMap = new HashMap<>();

    public StudentDAOImpl() {
        studentMap.put(1, new Student(1, "Student1", "City1", Calendar.getInstance()));
        studentMap.put(2, new Student(2, "Student2", "City2", Calendar.getInstance()));
        studentMap.put(3, new Student(3, "Student3", "City3", Calendar.getInstance()));
        studentMap.put(4, new Student(4, "Student4", "City3", Calendar.getInstance()));
        studentMap.put(5, new Student(5, "Student5", "City3", Calendar.getInstance()));
    }

    public Student findStudent(int id) {
        return studentMap.get(id);
    }

    public List<Student> findAllStudents() {
        return new ArrayList<>(studentMap.values());
    }

    public Student createStudent(String name, String city, Calendar birthDate) {
        int id = studentMap.size() + 1;
        Student student = new Student(id, name, city, birthDate);
        studentMap.put(id, student);
        return student;
    }

    public void deleteStudent(int id) {
        studentMap.remove(id);
    }

    public Student updateStudent(int id, String name, String city, Calendar birthDate) {
        return studentMap.put(id, new Student(id, name, city, birthDate));
    }
}
