package DAO;

import DTO.Student;

import java.util.Calendar;
import java.util.List;

public interface StudentDAO {
    Student findStudent(int id);
    List<Student> findAllStudents();
    Student createStudent(String name, String city, Calendar birthDate);
    void deleteStudent(int id);
    Student updateStudent(int id, String name, String city, Calendar birthDate);
}
