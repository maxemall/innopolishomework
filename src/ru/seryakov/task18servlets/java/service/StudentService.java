package service;


import DAO.StudentDAO;
import DTO.Student;

import java.util.Calendar;
import java.util.List;

public class StudentService {

    private StudentDAO studentDAO;

    public StudentService(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    public List<Student> findAllStudents() {
        return studentDAO.findAllStudents();
    }

    public Student findStudent(int id) {
        return studentDAO.findStudent(id);
    }

    public Student createStudent(String name, String city, Calendar birthDate) {
        return studentDAO.createStudent(name, city, birthDate);
    }

    public void removeStudent(int id) {
        studentDAO.deleteStudent(id);
    }
}
