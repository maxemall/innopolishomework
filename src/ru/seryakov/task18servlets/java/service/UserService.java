package service;


import DAO.UserDAO;
import DTO.User;

public class UserService {
    private UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public Boolean loginUser(String userName, String password) {
        for (User user: userDAO.findAllUsers()) {
            if (user != null && user.getUserName() != null && user.getPassword() != null
                    && (user.getUserName().equals(userName)) && (user.getPassword().equals(password))) {
                return true;
            }

        }
        return false;
    }
}
