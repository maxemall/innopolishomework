package service;


import DAO.GroupDAO;
import DTO.Group;
import DTO.Student;

import java.util.Calendar;
import java.util.List;

public class GroupService {

    private GroupDAO groupDAO;

    public GroupService(GroupDAO groupDAO) {
        this.groupDAO = groupDAO;
    }

    public List<Group> findAllGroups() {
        return groupDAO.findAllGroups();
    }

    public void addStudent(int id, Student student) {
        groupDAO.addStudent2Group(id, student);
    }

    public Student createAndAddStudent(int idGroup, String name, String city, Calendar birthDate) {
        Student student = ServiceFactory.getStudentService().createStudent(name, city, birthDate);
        addStudent(idGroup, student);
        return student;
    }

    public void removeStudent(int idGroup, int idStudent) {
        StudentService studentService = ServiceFactory.getStudentService();
        groupDAO.removeStudentFromGroup(idGroup, studentService.findStudent(idStudent));
        studentService.removeStudent(idStudent);
    }

    public List<Student> findStudentsByGroup(int id) {return groupDAO.findGroupStudents(id);}
}
