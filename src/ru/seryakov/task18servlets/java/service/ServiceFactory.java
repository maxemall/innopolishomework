package service;

import DAO.DaoFactory;

public class ServiceFactory {
    private static volatile UserService userService;
    private static volatile GroupService groupService;
    private static volatile StudentService studentService;

    public static UserService getUserService() {
        UserService localUserService = userService;

        if (localUserService == null) {
            synchronized (UserService.class) {
                localUserService = userService;
                if (localUserService == null) {
                    userService = localUserService = new UserService(DaoFactory.getUserDAO());
                }
            }
        }
        return userService;
    }

    public static StudentService getStudentService() {
        StudentService localStudentService = studentService;

        if (localStudentService == null) {
            synchronized (StudentService.class) {
                localStudentService = studentService;
                if (localStudentService == null) {
                    studentService = localStudentService = new StudentService(DaoFactory.getStudentDAO());
                }
            }
        }
        return studentService;
    }

    public static GroupService getGroupService() {
        GroupService localGroupService = groupService;

        if (localGroupService == null) {
            synchronized (GroupService.class) {
                localGroupService = groupService;
                if (localGroupService == null) {
                    groupService = localGroupService = new GroupService(DaoFactory.getGroupDAO());
                }
            }
        }
        return groupService;
    }
}
