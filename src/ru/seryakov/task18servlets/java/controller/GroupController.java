package controller;

import service.GroupService;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GroupController extends HttpServlet {
    private GroupService groupService;

    public GroupController() {
        this.groupService = ServiceFactory.getGroupService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("list", groupService.findAllGroups());
        req.getRequestDispatcher("/groups.jsp").forward(req, resp);
    }
}
