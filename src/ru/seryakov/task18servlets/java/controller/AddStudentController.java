package controller;

import service.GroupService;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

public class AddStudentController extends HttpServlet {

    private GroupService groupService;

    public AddStudentController() {
        this.groupService = ServiceFactory.getGroupService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String groupId = req.getParameter("groupId");
        String studentName = req.getParameter("studentName");
        String city = req.getParameter("city");
        if (city == null || city.equals("") || studentName.equals("") || studentName == null || groupId == null || groupId.equals("")) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        groupService.createAndAddStudent(Integer.valueOf(groupId), studentName, city, Calendar.getInstance());
        req.setAttribute("list", groupService.findStudentsByGroup(Integer.valueOf(groupId)));
        req.setAttribute("groupId", groupId);
        req.getRequestDispatcher("/students.jsp").forward(req, resp);
    }
}
