package controller;

import service.GroupService;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StudentsController extends HttpServlet {

    private GroupService groupService;

    public StudentsController() {
        this.groupService = ServiceFactory.getGroupService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String groupId = req.getParameter("groupId");
        if (null == groupId || groupId.equals("")) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        req.setAttribute("list", groupService.findStudentsByGroup(Integer.valueOf(groupId)));
        req.setAttribute("groupId", groupId);
        req.getRequestDispatcher("/students.jsp").forward(req, resp);
    }
}
