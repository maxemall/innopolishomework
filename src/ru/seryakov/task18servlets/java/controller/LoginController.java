package controller;

import service.ServiceFactory;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Создать веб-приложение "Студенты"
 *
 * На стартовом экране форма логина (ввод логина-пароля)
 *
 * После успешного входа в приложение попадаем на страницу со списком групп. Внизу форма добавления новой группы.
 * Есть кнопка выхода из профиля (сброс логина). Каждая группа в списке является ссылкой. При переходе по ссылке
 * попадаем на страницу со списком студентов группы. У каждого студента кнопка "удалить". Внизу форма добавления
 * нового студента (в текущую группу). Есть ссылка для возврата к списку групп.
 */
public class LoginController extends HttpServlet {
    private UserService userService;

    public LoginController() {
        userService = ServiceFactory.getUserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (userService.loginUser(req.getParameter("login"), req.getParameter("password"))) {
            req.getSession().setAttribute("login", true);
            req.getRequestDispatcher("/groups").forward(req, resp);
        } else {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }
}
