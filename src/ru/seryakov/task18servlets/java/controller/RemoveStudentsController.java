package controller;

import service.GroupService;
import service.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RemoveStudentsController extends HttpServlet {

    private GroupService groupService;

    public RemoveStudentsController() {
        this.groupService = ServiceFactory.getGroupService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String studentId = req.getParameter("studentId");
        String groupId = req.getParameter("groupId");
        if (studentId == null || studentId.equals("") || groupId == null || groupId.equals("")) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        groupService.removeStudent(Integer.valueOf(groupId), Integer.valueOf(studentId));
        req.setAttribute("groupId", groupId);
        req.setAttribute("list", groupService.findStudentsByGroup(Integer.valueOf(groupId)));
        req.getRequestDispatcher("/students.jsp").forward(req, resp);
    }
}
