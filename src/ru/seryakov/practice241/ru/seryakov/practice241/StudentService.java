package ru.seryakov.practice241;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.IOException;

public class StudentService {
    private Student student;
    private FileService fileService;
    private final Gson gson = new Gson();
    private final String FILE_PREFFIX = "student-";
    private final String FILE_SUFFIX = ".sv";
    private String pathToFile;

    public StudentService(Student student, String pathToFile) throws FileNotFoundException {
        this.student = student;
        this.pathToFile = pathToFile;
        fileService = new FileService(pathToFile + FILE_PREFFIX + student.getId().toString() + FILE_SUFFIX, 512);
    }

    public StudentService(Student student, FileService fileService, String pathToFile) {
        this.student = student;
        this.fileService = fileService;
        this.pathToFile = pathToFile;
    }

    public Boolean save() {
        try {
            fileService.write(gson.toJson(student));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Student get() {
        try {
            return student = gson.fromJson(fileService.read(), Student.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
