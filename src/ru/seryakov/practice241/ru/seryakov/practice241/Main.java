package ru.seryakov.practice241;

import com.google.gson.Gson;

import java.io.IOException;

public class Main {
    static Group group = new Group(1, "test group", 2019);
    static Student student = new Student(1, "Moscow", "Seryakoff MS", group);
    static Student student2 = new Student(2,"Moscow", "Ivanoff AA", group);
    static Student student3 = new Student(3, "DefaultCity", "Smirnoff VV", group);

    public static void main(String[] args) throws IOException {
        StudentService studentService = new StudentService(student, "/Users/maksim.seryakov/IdeaProjects/innopolishomework/src/ru/seryakov/practice241/ru/seryakov/practice241");
        studentService.save();

        StudentService studentService1 = new StudentService(student2, "/Users/maksim.seryakov/IdeaProjects/innopolishomework/src/ru/seryakov/practice241/ru/seryakov/practice241");
        studentService1.save();

        StudentService studentService2 = new StudentService(student2, "/Users/maksim.seryakov/IdeaProjects/innopolishomework/src/ru/seryakov/practice241/ru/seryakov/practice241");
        System.out.println(studentService.get());

    }
}

