package ru.seryakov.practice241;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Student {
    Integer id;
    String city;
    String fio;
    ru.seryakov.practice241.Group group;

    public Student(Integer id, String city, String fio, ru.seryakov.practice241.Group group) {
        this.id = id;
        this.city = city;
        this.fio = fio;
        this.group = group;
    }
}
