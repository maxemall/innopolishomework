package ru.seryakov.practice241;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {


    String studentJson = "{\"id\":2,\"city\":\"Moscow\",\"fio\":\"Ivanoff AA\",\"group\":{\"id\":1,\"name\":\"test group" +
            "\",\"GraduationYear\":2019}}";
    Student student;

    @Mock
    FileService fileService;

    @InjectMocks
    StudentService subject;

    public StudentServiceTest() throws FileNotFoundException {
    }

    @Before
    public void setUp() throws Exception {

        Group group = new Group(1, "test group", 2019);
        student = new Student(2,"Moscow", "Ivanoff AA", group);
        subject = new StudentService(student, fileService, "fileName");

    }

    @Test
    public void saveSuccess() {
        assertTrue(subject.save());
    }

    @Test
    public void saveError() throws IOException {
        //вот тут как принято поступать?
        //да
        //я понял. Я не жду тут эксепшн. Он заставляет его обработать, вот красненькое ниже :)
        //ок, ну и сами тесты можно покритиковать

            doThrow(new IOException()).when(fileService).write(anyString());

        Boolean saveResult = subject.save();
        assertFalse(saveResult);
    }

    @Test
    public void get() throws IOException {

        when(fileService.read()).thenReturn(studentJson);

        assertEquals(subject.get(), student);
        //что запустить?
        //ок, понял. Спасибо.
    }
}