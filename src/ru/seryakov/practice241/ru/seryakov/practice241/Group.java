package ru.seryakov.practice241;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Group {
    Integer id;
    String name;
    Integer GraduationYear;

    public Group(Integer id, String name, Integer graduationYear) {
        this.id = id;
        this.name = name;
        GraduationYear = graduationYear;
    }
}
