package ru.seryakov.task11;

import java.math.BigDecimal;

//    программу, которая считает зарплату «на руки» (на вход программе передается величина зарплаты, на выходе печатается
//    зарплата за вычетом 13% (НДФЛ). Пример: на вход подается 70000, на выходе печатается 60900 руб.
class NetSalary {
    public static void main(String[] args) {
        try {
            BigDecimal grossSalary = new BigDecimal(args[0]);
            BigDecimal ndfl = new BigDecimal(0.13);
            BigDecimal netSalary = grossSalary.multiply(ndfl);
            System.out.println("Pure salary " + netSalary);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("You must specify arguments.");
        } catch (NumberFormatException e) {
            System.out.println("Arguments illegal type.");
        }
    }
}

