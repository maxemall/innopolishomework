package ru.seryakov.task11;

import java.math.BigDecimal;

//программу, которая считает стоимость бензина (на вход программе передается кол-во литров, на выходе печатается стоимость).
//        Пример: стоимость литра бензина 43 рубля. На вход подается 50, на выходе должно быть 2150 руб.
class FuelCost {
    public static void main(String[] args) {
        try {
            BigDecimal price = new BigDecimal(43);
            BigDecimal value = new BigDecimal(args[0]);
            BigDecimal cost = price.multiply(value);
            System.out.println("Cost is " + cost);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("You must specify arguments.");
        } catch (NumberFormatException e) {
            System.out.println("Arguments illegal type.");
        }
    }
}

