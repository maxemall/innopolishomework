package ru.seryakov.task11;

//программу, которая конвертирует секунды в часы. Пример: на вход подается 3600, на выходе печатается 1 час.
class SecondsConverter {
    public static void main(String[] args) {
        try {
            int seconds = Integer.parseInt(args[0]);
            float hours = seconds / 3600f;
            System.out.println(String.valueOf(seconds) + " seconds is " + hours + " hours");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("You must specify arguments.");
        } catch (NumberFormatException e) {
            System.out.println("Arguments illegal type.");
        }
    }
}

