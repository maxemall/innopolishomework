package ru.seryakov.task42;

public class IntegerBox extends AbstractBox<Integer> {

    public IntegerBox(Integer[] numbers) {
        super(numbers);
    }

    @Override
    protected Integer sum(Integer numberA, Integer numberB) {
        return numberA + numberB;
    }

    @Override
    protected Integer divide(Integer numerator, Integer denominator) {
        return numerator/denominator;
    }


}
