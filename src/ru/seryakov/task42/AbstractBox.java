package ru.seryakov.task42;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class AbstractBox<E extends Number> implements MathBox<E> {

    private Set<E> numbersSet = new HashSet<>();

    public AbstractBox(E[] numbers) {
        numbersSet.addAll(Arrays.asList(numbers));
    }

    @Override
    public E summator() {
        E result = null;
        for (E number : numbersSet
        ) {
            result = sum(result, number);
        }
        return result;
    }

    protected abstract E sum(E numberA, E numberB);

    @Override
    public void splitter(E denominator) {
        Set<E> bufferSet = new HashSet<>();

        for (E number:numbersSet) {
            bufferSet.add(divide(number, denominator));
        }

        numbersSet = bufferSet;
    }

    protected abstract E divide(E numerator, E denominator);

    @Override
    public String toString() {
        return "AbstractBox{" +
                "numbersSet=" + numbersSet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractBox<?> that = (AbstractBox<?>) o;
        return Objects.equals(numbersSet, that.numbersSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbersSet);
    }

    public Set<E> getNumbersSet() {
        return numbersSet;
    }
}
