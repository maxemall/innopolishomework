package ru.seryakov.task42;

import java.math.BigDecimal;

public class BigDecimalBox extends AbstractBox<BigDecimal> {


    public BigDecimalBox(BigDecimal[] numbers) {
        super(numbers);
    }

    @Override
    protected BigDecimal sum(BigDecimal numberA, BigDecimal numberB) {
        return numberA.add(numberB);
    }

    @Override
    protected BigDecimal divide(BigDecimal numerator, BigDecimal denominator) {
        return numerator.divide(denominator);
    }


}
