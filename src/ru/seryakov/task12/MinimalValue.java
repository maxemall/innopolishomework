package ru.seryakov.task12;

import java.util.Scanner;

// 2) Написать программу для поиска минимального из 4 чисел.
class MinimalValue {
    public static void main(String[] args) {
        final int limit = 4;
        int[] numbers = new int[4];
        getNumbers(numbers, limit);
        System.out.println("Minimal value is " + getMinVal(numbers));
    }

    private static void getNumbers(int[] numbers, int limit) {
        System.out.println("Input " + limit + " values to compare:");
        Scanner scanner = new Scanner(System.in);
        try {
            for (int i = 0; i < limit; i++) {
                numbers[i] = scanner.nextInt();
            }
        } catch (NumberFormatException e) {
            System.out.println("Arguments illegal type.");
        }
    }

    private static int getMinVal(int[] numbers) {
        int minVal = numbers[0];

        for (int i = 1; i < 4; i++) {
            minVal = (minVal < numbers[i] ? minVal : numbers[i]);
        }
        return minVal;
    }
}


